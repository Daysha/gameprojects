﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.MazeGenerator
{
    class Cell
    {
        #region private atributes
        private int x;
        private int y;
        private int output;
        private bool visited;
        private bool[] walls;
        #endregion


        #region Properties
        public int X { get => x; private set => x = value; }
        public int Y { get => y; private set => y = value; }
        public int Output { get => output; set => output = value; }
        public bool Visited { get => visited; set => visited = value; }
        public bool[] Walls { get => walls; set => walls = value; }
        #endregion

        public Cell(int x, int y)
        {
            X = x;
            Y = y;
            Output = 2;
            Visited = false;
            Walls = new bool[]{true,true,true,true};
        }
    }
}
