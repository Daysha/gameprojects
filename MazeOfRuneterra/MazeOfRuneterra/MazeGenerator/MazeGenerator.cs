﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.MazeGenerator
{
    class MazeGenerator
    {
        #region private atributes
        private Cell[,] maze; // Zwischendaten für das Maze.
        private int[,] mazeData; // Daten für das fertige Maze.

        private int width;
        private int height;

        Random random;

        private Cell current;
        private Stack<Cell> backtracker;
        #endregion


        #region Properties
        public Cell[,] Maze { get => maze; private set => maze = value; }
        public int[,] MazeData { get => mazeData; private set => mazeData = value; }
        public int Width { get => width; private set => width = value; }
        public int Height { get => height; private set => height = value; }
        internal Stack<Cell> Backtracker { get => backtracker; set => backtracker = value; }
        internal Cell Current { get => current; set => current = value; }
        public Random Random { get => random; private set => random = value; }
        #endregion

        /// <summary>
        /// Initialisiert und generiert die Date für ein Maze.
        /// </summary>
        public MazeGenerator(int width, int height)
        {
            Width = width;
            Height = height;
            Maze = new Cell[Width, Height];
            MazeData = new int[Width * 3 - (Width - 1), Height * 3 - (Height - 1)];
            Backtracker = new Stack<Cell>();
            Random = new Random();
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    Maze[x, y] = new Cell(x,y);
                }
            }

            Current = Maze[0, 1];
            backtracker.Push(Current);
            while (backtracker.Count > 0)
            {
                Current.Visited = true;
                Current.Output = 0;
                List<Cell> neighbours = GetNeighbours(Current);
                if (neighbours.Count > 0)
                {
                    int rnd = Random.Next(0,neighbours.Count);
                    Cell temp = neighbours.ElementAt(rnd);
                    neighbours.RemoveAt(rnd);
                    temp.Visited = true;
                    backtracker.Push(Current);
                    ChangeWalls(Current,temp);
                    Current = temp;
                }
                else if (backtracker.Count > 0)
                {
                    Current = backtracker.Pop();
                }
            }
            ConvertToMazeMap();

        }

        /// <summary>
        /// Verändert die boolischen Werte der Walls Eigenschaft anhand der übergebenen Zellen.
        /// </summary>
        private void ChangeWalls(Cell current, Cell next)
        {
            int x = current.X - next.X;
            int y = current.Y - next.Y;

            if (x == 1)
            {
                current.Walls[3] = false;
                next.Walls[1] = false;
            }
            else if (x == -1)
            {
                current.Walls[1] = false;
                next.Walls[3] = false;
            }

            if (y == 1)
            {
                current.Walls[0] = false;
                next.Walls[2] = false;
            }
            else if (y == -1)
            {
                current.Walls[2] = false;
                next.Walls[0] = false;
            }
        }
        
        /// <summary>
        /// Überprüft welche Nachbar die übergebene Zelle hat.
        /// </summary>
        private List<Cell> GetNeighbours(Cell cell)
        {
            List<Cell> neighbours = new List<Cell>();
            int x = cell.X;
            int y = cell.Y;

            if (y - 1 >= 0 && !Maze[x, y - 1].Visited)
            {
                neighbours.Add(Maze[x, y - 1]);
            }
            if (x + 1 < Width && !Maze[x + 1, y].Visited)
            {
                neighbours.Add(Maze[x + 1, y]);
            }
            if (y + 1 < Height && !Maze[x, y + 1].Visited)
            {
                neighbours.Add(Maze[x, y + 1]);
            }
            if (x - 1 >= 0 && !Maze[x - 1, y].Visited)
            {
                neighbours.Add(Maze[x - 1, y]);
            }

            return neighbours;
        }
        

        /// <summary>
        /// Konvertiert die Daten aus den Zwischendaten in eine Map für das Maze.
        /// </summary>
        public void ConvertToMazeMap()
        {
            string output = "";

            for (int y = 0; y < Height; y++)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int x = 0; x < Width; x++)
                    {
                        if (i % 2 == 0)
                        {
                            if (Maze[x, y].Walls[i])
                            {
                                output += "1 | 1 | 1 | ";
                            }
                            else
                            {
                                output += "1 | 0 | 1 | ";
                            }
                        }
                        else
                        {
                            if (Maze[x, y].Walls[i + 2])
                            {
                                output += "1 | ";
                            }
                            else
                            {
                                output += "0 | ";
                            }
                            if (Maze[x, y].Walls[i])
                            {
                                output += "0 | 1 | ";
                            }
                            else
                            {
                                output += "0 | 0 | ";
                            }
                        }
                    }
                    output += "\n";
                }
            }

            string[] str_mazeData = output.Split(new char[] {' ','|'},StringSplitOptions.RemoveEmptyEntries);

            int countX = 0;
            int countY = 0;

            for (int y = 0; y < Height*3; y++)
            {
                if (!(y == Height * 3 - 1) && y % 3 == 2)
                {
                    countY++;
                    continue;
                }
                countX = 0;
                for (int x = 0; x < Width*3; x++)
                {
                    
                    if (!(x == Width * 3 - 1) && x%3 == 2)
                    {
                        countX++;
                    }
                    else
                    {
                       MazeData[x - countX, y - countY] = Convert.ToInt32(str_mazeData[Height * 3 * y + x]);
                    }
                }
            }
            MazeData[0, 1] = 2;
            MazeData[MazeData.GetLength(1) - 1, MazeData.GetLength(0) - 2] = 3;
            for (int y = 0; y < MazeData.GetLength(0); y++)
            {
                for (int x = 0; x < MazeData.GetLength(1); x++)
                {
                    Console.Write(MazeData[x, y] + " | ");
                }
                Console.Write("\n");
            }
        }

        
    }
}
