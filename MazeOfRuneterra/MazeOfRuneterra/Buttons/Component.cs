﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.Buttons
{
    public abstract class Component : IGameComponent
    {
        #region Methoden
        public abstract void Draw(SpriteBatch spriteBatch);
        public abstract void Initialize();
        public abstract void Update(GameTime gameTime, Game game);
        #endregion
    }
}
