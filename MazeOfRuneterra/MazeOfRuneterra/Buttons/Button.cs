﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.Buttons
{
    class Button : Component, IGameComponent
    {
        #region Felder
        private MouseState _currentMouse;

        private SpriteFont _font;
        private bool _isHovering;

        private MouseState _previousMouse;

        private Texture2D _texture;
        #endregion

        #region Eigenschaften

        public event EventHandler Click;

        public bool Clicked { get; private set; }

        public Color PenColour { get; set; }

        public Vector2 Position { get; set; }

        public Rectangle Rectangle
        {
            get
            {
                return new Rectangle((int)Position.X - 100, (int)Position.Y - 190, _texture.Width, _texture.Height);
            }
        }

        public string Text { get; set; }
        #endregion

        #region Methoden

        public Button(Texture2D texture, SpriteFont font)
        {
            _texture = texture;
            _font = font;

            //   Rectangle = new Rectangle((int)Position.X,(int)Position.Y,(int)_font.MeasureString(Text).X, (int)_font.MeasureString(Text).Y);

        }
        

        public override void Initialize()
        {

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            var colour = Color.White;

            if (_isHovering)
            {
                colour = Color.Green;
            }

            spriteBatch.Draw(_texture, Rectangle, colour);

            if (!string.IsNullOrEmpty(Text))
            {
                var x = (Rectangle.X + (Rectangle.Width / 2)) - (_font.MeasureString(Text).X / 2);
                var y = (Rectangle.Y + (Rectangle.Height / 2)) - (_font.MeasureString(Text).X / 2);

                spriteBatch.DrawString(_font, Text, new Vector2(x, y + 190), Color.White);

            }
        }

        public override void Update(GameTime gameTime, Game game)
        {
            Console.WriteLine(_texture.Width);
            Console.WriteLine(Mouse.GetState());
            _previousMouse = _currentMouse;
            _currentMouse = Mouse.GetState();

            var mouseRectangle = new Rectangle(_currentMouse.X, _currentMouse.Y, 1, 1);

            _isHovering = false;

            if (mouseRectangle.Intersects(Rectangle))
            {
                _isHovering = true;

                if (_currentMouse.LeftButton == ButtonState.Released && _previousMouse.LeftButton == ButtonState.Pressed)
                {
                    Click?.Invoke(this, new EventArgs());


                }
            }
        }

        #endregion
    }
}

