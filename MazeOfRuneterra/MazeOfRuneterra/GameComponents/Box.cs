﻿using MazeOfRuneterra.GameComponent;
using MazeOfRuneterra.Kamera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.GameComponents
{
    class Box : BasicGameComponent
    {
        #region Attribute

        private VertexPositionNormalTexture[] vertices;
        private BasicEffect effect;
        private VertexBuffer vertexBuffer;
        public IndexBuffer indexBuffer;
        private float TextureWidth;
        private float TextureHeight;
        private Matrix worldeffekt;


        //Vector Box forground
        private Vector3 vFgLeftDown;
        private Vector3 vFgLeftUp;
        private Vector3 vFgRightDown;
        private Vector3 vFgRightUp;


        //Vector Box Background       
        private Vector3 vBgLeftDown;
        private Vector3 vBgLeftUp;
        private Vector3 vBgRightDown;
        private Vector3 vBgRightUp;

        //Texture Vectors
        private Vector2 tLeftBorderLeftDown;
        private Vector2 tLeftBorderLeftUp;
        private Vector2 tLeftBorderRightDown;
        private Vector2 tLeftBorderRightUp;
        private Vector2 tRightBorderLeftDown;
        private Vector2 tRightBorderLeftUp;
        private Vector2 tRightBorderRightDown;
        private Vector2 tRightBorderRightUp;
        private Vector2 tUpBorderLeft;
        private Vector2 tUpBorderRight;
        private Vector2 tDownBorderLeft;
        private Vector2 tDownBorderRight;
        private Vector2 t2MiddelLeftDown;
        private Vector2 t2MiddelLeftUp;

        private float textureBlockSize;

        public BoundingBox BoundingBox;

        private Texture2D texture;
        private Vector3 position;
        private Vector3 size;

        public Vector3 Position
        {
            get { return position; }
            set
            {
                position = value;
                SetVectorsPosition();
                SetTextureVectors();
                SetVerticesPosition();
                SetTriangles();
            }
        }

        public Vector3 Size
        {
            get { return size; }
            set
            {
                size = value;
                SetVectorsPosition();
                SetTextureVectors();
                SetVerticesPosition();
                SetTriangles();
            }
        }

        public Texture2D Texture
        {
            get { return texture; }
            private set
            {
                texture = value;
            }
        }
        #endregion


        #region Konstruktor
        public Box(Game game, ICamera camera, Vector3 position, Vector3 size, string texture, float textureBlocksize) : base(game, camera)
        {
            this.position = position;
            this.size = size;
            Texture = Game.Content.Load<Texture2D>(texture);
            TextureWidth = Texture.Width;
            TextureHeight = Texture.Height;
            this.textureBlockSize = textureBlocksize;

            effect = new BasicEffect(Game.GraphicsDevice);
            effect.Texture = Texture;
            effect.TextureEnabled = true;
            //effect.EnableDefaultLighting();
            worldeffekt = Matrix.Identity;

            SetVectorsPosition();
            SetTextureVectors();
            SetVerticesPosition();
            SetTriangles();

            BoundingBox = new BoundingBox(vBgLeftDown, vFgRightUp);
        }

        public override void Draw(GameTime gameTime)
        {
            effect.World = worldeffekt;
            effect.Projection = Camera.Projection;
            effect.View = Camera.View;
            effect.CurrentTechnique.Passes[0].Apply();
            Game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            Game.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            Game.GraphicsDevice.Indices = indexBuffer;
            Game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 12);
            base.Draw(gameTime);
        }

        private void SetVectorsPosition()
        {
            vFgLeftDown = new Vector3(position.X - Size.X, position.Y - Size.Y, position.Z + Size.Z);
            vFgLeftUp = new Vector3(position.X - Size.X, position.Y + Size.Y, position.Z + Size.Z);
            vFgRightDown = new Vector3(position.X + Size.X, position.Y - Size.Y, position.Z + Size.Z);
            vFgRightUp = new Vector3(position.X + Size.X, position.Y + Size.Y, position.Z + Size.Z);

            vBgLeftDown = new Vector3(position.X - Size.X, position.Y - Size.Y, position.Z - Size.Z);
            vBgLeftUp = new Vector3(position.X - Size.X, position.Y + Size.Y, position.Z - Size.Z);
            vBgRightDown = new Vector3(position.X + Size.X, position.Y - Size.Y, position.Z - Size.Z);
            vBgRightUp = new Vector3(position.X + Size.X, position.Y + Size.Y, position.Z - Size.Z);

         }

        private void SetTextureVectors()
        {
            tLeftBorderLeftDown = new Vector2(0, textureBlockSize * 2 / TextureHeight);
            tLeftBorderLeftUp = new Vector2(0, textureBlockSize / TextureHeight);
            tLeftBorderRightDown = new Vector2(textureBlockSize / TextureWidth, textureBlockSize * 2 / TextureHeight);
            tLeftBorderRightUp = new Vector2(textureBlockSize / TextureWidth, textureBlockSize / TextureHeight);
            tRightBorderLeftDown =  new Vector2(textureBlockSize * 3 / TextureWidth, textureBlockSize * 2 / TextureHeight);
            tRightBorderLeftUp = new Vector2(textureBlockSize * 3 / TextureWidth, textureBlockSize / TextureHeight);
            tRightBorderRightDown = new Vector2(1, textureBlockSize * 2 / TextureHeight);
            tRightBorderRightUp = new Vector2(1, textureBlockSize / TextureHeight);
            tUpBorderLeft = new Vector2(textureBlockSize / TextureWidth, 0);
            tUpBorderRight = new Vector2(textureBlockSize * 2 / TextureWidth, 0);
            tDownBorderLeft = new Vector2(textureBlockSize / TextureWidth, 1);
            tDownBorderRight = new Vector2(textureBlockSize * 2 / TextureWidth, 1);
            t2MiddelLeftDown = new Vector2(textureBlockSize * 2 / TextureWidth, textureBlockSize * 2 / TextureHeight);
            t2MiddelLeftUp = new Vector2(textureBlockSize * 2 / TextureWidth, textureBlockSize / TextureHeight);
        }

        private void SetVerticesPosition()
        {
            vertices = new VertexPositionNormalTexture[14];


            vertices[0] = new VertexPositionNormalTexture(vFgLeftDown, Vector3.Up, tLeftBorderRightDown);
            vertices[1] = new VertexPositionNormalTexture(vFgLeftUp, Vector3.Up, tLeftBorderRightUp);
            vertices[2] = new VertexPositionNormalTexture(vFgRightDown, Vector3.Up, t2MiddelLeftDown);
            vertices[3] = new VertexPositionNormalTexture(vFgRightUp, Vector3.Up, t2MiddelLeftUp);

            vertices[4] = new VertexPositionNormalTexture(vBgLeftUp, Vector3.Up, tRightBorderRightUp);
            vertices[5] = new VertexPositionNormalTexture(vBgLeftDown, Vector3.Up, tRightBorderRightDown);
            vertices[6] = new VertexPositionNormalTexture(vBgRightDown, Vector3.Up, tRightBorderLeftDown);
            vertices[7] = new VertexPositionNormalTexture(vBgRightUp, Vector3.Up, tRightBorderLeftUp);

            vertices[8] = new VertexPositionNormalTexture(vBgLeftUp, Vector3.Up, tLeftBorderLeftUp);
            vertices[9] = new VertexPositionNormalTexture(vBgLeftDown, Vector3.Up, tLeftBorderLeftDown);

            vertices[10] = new VertexPositionNormalTexture(vBgLeftUp, Vector3.Up, tUpBorderLeft);
            vertices[11] = new VertexPositionNormalTexture(vBgRightUp, Vector3.Up, tUpBorderRight);

            vertices[12] = new VertexPositionNormalTexture(vBgLeftDown, Vector3.Up, tDownBorderLeft);
            vertices[13] = new VertexPositionNormalTexture(vBgRightDown, Vector3.Up, tDownBorderRight);


            vertexBuffer = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPositionNormalTexture), vertices.Length, BufferUsage.WriteOnly);
            vertexBuffer.SetData<VertexPositionNormalTexture>(vertices);
        }


        public virtual void SetTriangles()
        {
            int j = 0;
            short[] triangle = new short[12 * 6]; // Anzahl der Dreiecke

            //Vorne
            triangle[j++] = 0;
            triangle[j++] = 1;
            triangle[j++] = 3;

            triangle[j++] = 3;
            triangle[j++] = 2;
            triangle[j++] = 0;

            //Linke Seite

            triangle[j++] = 1;
            triangle[j++] = 0;
            triangle[j++] = 9;

            triangle[j++] = 1;
            triangle[j++] = 9;
            triangle[j++] = 8;
			
            //Hinten
            triangle[j++] = 7;
            triangle[j++] = 4;
            triangle[j++] = 5;

            triangle[j++] = 5;
            triangle[j++] = 6;
            triangle[j++] = 7;

            //Rechte Seite
            triangle[j++] = 3;
            triangle[j++] = 7;
            triangle[j++] = 6;

            triangle[j++] = 6;
            triangle[j++] = 2;
            triangle[j++] = 3;

            //Boden
            triangle[j++] = 13;
            triangle[j++] = 12;
            triangle[j++] = 0;

            triangle[j++] = 0;
            triangle[j++] = 2;
            triangle[j++] = 13;

            //Deckel
            triangle[j++] = 1;
            triangle[j++] = 10;
            triangle[j++] = 11;

            triangle[j++] = 11;
            triangle[j++] = 3;
            triangle[j++] = 1;



            indexBuffer = new IndexBuffer(Game.GraphicsDevice, typeof(short), triangle.Length, BufferUsage.WriteOnly);
            indexBuffer.SetData<short>(triangle);
        }
        #endregion
    }
}
