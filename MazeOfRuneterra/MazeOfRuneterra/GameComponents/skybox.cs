﻿using MazeOfRuneterra.Kamera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.GameComponents
{
    class Skybox : Box
    {

        public Skybox(Game game, ICamera camera, Vector3 position, Vector3 size, string texture, float textureBlocksize) : base(game, camera, position,size, texture,textureBlocksize)
        {
            SetTriangles();
        }

        public override void SetTriangles()
        {
            int j = 0;
            short[] triangle = new short[12 * 6]; // Anzahl der Dreiecke

            //Vorne
            triangle[j++] = 3;
            triangle[j++] = 1;
            triangle[j++] = 0;

            triangle[j++] = 0;
            triangle[j++] = 2;
            triangle[j++] = 3;

            //Linke Seite

            triangle[j++] = 9;
            triangle[j++] = 0;
            triangle[j++] = 1;

            triangle[j++] = 8;
            triangle[j++] = 9;
            triangle[j++] = 1;

            //Hinten
            triangle[j++] = 5;
            triangle[j++] = 4;
            triangle[j++] = 7;

            triangle[j++] = 7;
            triangle[j++] = 6;
            triangle[j++] = 5;

            //Rechte Seite
            triangle[j++] = 6;
            triangle[j++] = 7;
            triangle[j++] = 3;

            triangle[j++] = 3;
            triangle[j++] = 2;
            triangle[j++] = 6;

            //Boden
            triangle[j++] = 0;
            triangle[j++] = 12;
            triangle[j++] = 13;

            triangle[j++] = 13;
            triangle[j++] = 2;
            triangle[j++] = 0;

            //Deckel
            triangle[j++] = 11;
            triangle[j++] = 10;
            triangle[j++] = 1;

            triangle[j++] = 1;
            triangle[j++] = 3;
            triangle[j++] = 11;



            indexBuffer = new IndexBuffer(Game.GraphicsDevice, typeof(short), triangle.Length, BufferUsage.WriteOnly);
            indexBuffer.SetData<short>(triangle);
        }
    }
}
