﻿using MazeOfRuneterra.Kamera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.GameComponent
{
    class Plane : BasicGameComponent
    {
        #region Attribute
        private Texture2D texture;
        private Vector3 position;
        private Vector3 size;
        private VertexPositionNormalTexture[] vertices;
        private BasicEffect effect;
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        private float textureWidth;
        private float textureHeight;
        private Matrix worldeffekt;
        private BoundingBox bbx;


        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }

        public Vector3 Size
        {
            get { return size; }
            set { size = new Vector3(value.X, value.Y, 0); }
        }

        public Texture2D Texture
        {
            get { return texture; }
            private set
            {
                texture = value;
            }
        }

        public BoundingBox Bbx { get => bbx; set => bbx = value; }

        #endregion
        #region Konstructor/ Methods

        public Plane(Game game, ICamera camera, Vector3 position, Vector3 size, string texture) : base(game,camera)
        {

            Position = position;
            Size = size;

            bbx = new BoundingBox(Position + new Vector3(0, -0.1f, 0), Position+Size+new Vector3(0,0.1f,0));

            Texture = Game.Content.Load<Texture2D>(texture);

            effect = new BasicEffect(Game.GraphicsDevice);
            effect.Texture = Texture;
            effect.TextureEnabled = true;
            effect.EnableDefaultLighting();
            worldeffekt = Matrix.Identity; // * Matrix.CreateScale(5);

            SetVertices();
            SetTriangles();
        }
        public Plane(Game game, ICamera camera, Vector3 position, string texture) : base(game, camera)
        {

            Position = position;
            Size = new Vector3(1,1,0);

            Texture = Game.Content.Load<Texture2D>(texture);

            effect = new BasicEffect(Game.GraphicsDevice);
            effect.Texture = Texture;
            effect.TextureEnabled = true;
            effect.EnableDefaultLighting();
            worldeffekt = Matrix.Identity; // * Matrix.CreateScale(5);

            SetVertices();
            SetTriangles();
        }

        public override void Draw(GameTime gameTime)
        {
            effect.World = worldeffekt;
            effect.Projection = Camera.Projection;
            effect.View = Camera.View;
            effect.CurrentTechnique.Passes[0].Apply();

            Game.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            Game.GraphicsDevice.Indices = indexBuffer;
            Game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 200);

            base.Draw(gameTime);
        }

        public void Rotation(float radius, string orientation)
        {
            float rotation = MathHelper.ToRadians(radius);
            
            switch (orientation.ToUpper())
            {
                    case "Z" :  worldeffekt = Matrix.CreateRotationZ(rotation);break;
                    case "Y" :  worldeffekt = Matrix.CreateRotationY(rotation); break;
                    case "X" :  worldeffekt = Matrix.CreateRotationX(rotation); break;
            }
        }

        public void Scale(float scalefactor)
        {
            worldeffekt = worldeffekt * Matrix.CreateScale(scalefactor);
        }

        private void SetVertices()
        {
            vertices = new VertexPositionNormalTexture[((int)Size.X + 1) * ((int)Size.Y + 1)];

            for (int row = 0; row <= Size.Y; row++)
            {
                for (int column = 0; column <= Size.X; column++)
                {
                    vertices[column + row * ((int)Size.X + 1)] = new VertexPositionNormalTexture(
                                                                        new Vector3(-Position.X / 2 + column, position.Y, -Position.Z / 2 + row),
                                                                        Vector3.Up,
                                                                        new Vector2(((float)row / Size.X), 1 - ((float)column / Size.Y)));
                }
            }

            vertexBuffer = new VertexBuffer(Game.GraphicsDevice, typeof(VertexPositionNormalTexture), vertices.Length, BufferUsage.WriteOnly);
            vertexBuffer.SetData<VertexPositionNormalTexture>(vertices);
        }

        private void SetTriangles()
        {
            int[] triangles = new int[((int)Size.X) * ((int)Size.Y) * 6];
            int i = 0;

            for (int row = 0; row < Size.Y; row++)
            {
                for (int column = 0; column < Size.X; column++)
                {
                    int lowerLeft = column + row * ((int)Size.X + 1);
                    int lowerRight = lowerLeft + 1;
                    int upperLeft = lowerLeft + (int)Size.X + 1;
                    int upperRight = upperLeft + 1;

                    // Specify upper triangle
                    triangles[i++] = lowerLeft;
                    triangles[i++] = upperRight;
                    triangles[i++] = upperLeft;

                    // Specify lower triangle
                    triangles[i++] = lowerRight;
                    triangles[i++] = upperRight;
                    triangles[i++] = lowerLeft;
                }
            }

            indexBuffer = new IndexBuffer(Game.GraphicsDevice, typeof(int), triangles.Length, BufferUsage.WriteOnly);
            indexBuffer.SetData<int>(triangles);
        }

        public void SetTexture(String texture)
        {
            Texture = Game.Content.Load<Texture2D>(texture);
        }
        #endregion

    }
}
