﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazeOfRuneterra.Kamera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace MazeOfRuneterra.GameComponent
{
    class  BasicGameComponent : DrawableGameComponent
    {
        #region Attribute
        public ICamera Camera { get; }
        public VertexPositionNormalTexture[] Vertices { get; }
        public BasicEffect Effect { get; }
        public VertexBuffer VertexBuffer { get; }
        public IndexBuffer IndexBuffer { get; }
        public float TextureWidth { get; }
        public float TextureHeight{ get; }
        private Matrix worldeffekt;
        private Texture2D texture;
        private Vector3 position;
        private Vector3 size;
        #endregion

        #region Konstruktor
        public BasicGameComponent (Game game, ICamera camera) : base(game)
        {
            this.Camera = camera;
         }
        #endregion

    }
}
