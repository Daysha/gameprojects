﻿using MazeOfRuneterra.GameComponent;
using MazeOfRuneterra.Kamera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.GameComponents
{
    class Hud : BasicGameComponent
    {

      
        public BasicEffect Effect { get; }
        private Matrix worldeffekt;
        private SpriteFont timeFont;
        SpriteBatch spriteBatch;
        private string time;
        private int mins = 0;
        private int secs = 0;
        private float elapsedTime = 0;



        #region Konstruktor
        public Hud(Game game, ICamera camera) : base (game,camera)
        {
           
        }
        #endregion

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            timeFont = Game.Content.Load<SpriteFont>("Font/TimerFontCeltic");
            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            setTimer(gameTime);
            spriteBatch.Begin();
            spriteBatch.DrawString(timeFont, time, new Vector2(Game.GraphicsDevice.Viewport.Width / 2 - (timeFont.Texture.Width / 4), 5), Color.Black, 0, Vector2.Zero, 1f, SpriteEffects.None, 1);
            //spriteBatch.DrawString(scoreFont, scoreRight.ToString("00"), new Vector2(GraphicsDevice.Viewport.Width / 2 + 90, 100), Color.White);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void setTimer(GameTime gameTime)
        {
            elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            mins = ((int)elapsedTime) / 60;
            secs = ((int)elapsedTime) % 60;

            if (secs < 10)
            {
                if (mins < 10)
                {
                    time = $"0{mins}m:0{secs}s";
                }
                else
                {
                    time = $"{mins}m:0{secs}s";
                }
            }
            else
            {
                if (mins < 10)
                {
                    time = $"0{mins}m:{secs}s";
                }
                else
                {
                    time = $"{mins}m:{secs}s";
                }
            }
        }
    }
}
