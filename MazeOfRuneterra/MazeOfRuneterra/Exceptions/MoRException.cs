﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.Exceptions
{
    public class MoRException : Exception
    {
        #region Variablen
        private Exception caughtError;
        private string errorDescription;


        public Exception CaughtError
        {
            get; private set;
        }

        public string ErrorDescription
        {
            get; private set;
        }

        #endregion

        #region Konstruktoren
        public MoRException(Exception error, string errorDescription)
        {
            CaughtError = error;
            ErrorDescription = errorDescription;

            Console.WriteLine($"Exception in {ErrorDescription}: {CaughtError.Message}");
        }

        public MoRException(Exception error)
        {
            CaughtError = error;
            ErrorDescription = error.Message;

            Console.WriteLine($"Error: {ErrorDescription}");
        }

        #endregion
    }
}
