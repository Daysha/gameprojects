﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.GameStateManager
{
    public abstract class GameState : IGameState
    {
        #region Felder
        protected GraphicsDevice _graphicsDevice;
        protected GraphicsDeviceManager _graphicsDeviceManager;
        protected Game _game;
        private ContentManager cm;
        #endregion

        #region Eigenschaften
        public ContentManager Cm
        {
            get
            {
                return cm;
            }
            set
            {
                if (value != null)
                    cm = value;
            }
        }
        #endregion

        #region Methoden
        public GameState(Game game, GraphicsDevice graphicsDevice, GraphicsDeviceManager graphicsDeviceManager)
        {
            _graphicsDevice = graphicsDevice;
            _graphicsDeviceManager = graphicsDeviceManager;
            _game = game;
            Cm = _game.Content;
        }

        public abstract void Initialize();
        public abstract void LoadContent(ContentManager content);
        public abstract void UnloadContent();
        public abstract void Update(GameTime gameTime);
        public abstract void Draw(GameTime gameTime);


        #endregion
    }
}
