﻿using MazeOfRuneterra.GameStateManager;
using MazeOfRuneterra.GameStateManager.GameStates;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeOfRuneterra.GameStateManaging.GameStates
{
    class DifficultySelectionState : GameState
    {
        private SpriteBatch spriteBatch;
        private Texture2D[] grad;
        private Texture2D screen;
        private KeyboardState lastFrameKeyboardState;

        private int texture = 0;
        private float inputDelay = 0.6f;
        private float elapsedTime = 0;

        public DifficultySelectionState(Game game, GraphicsDevice _graphicsDevice, GraphicsDeviceManager _graphicsDeviceManager) : base (game,_graphicsDevice,_graphicsDeviceManager)
        {
            
        }

        public override void Initialize()
        {
            spriteBatch = new SpriteBatch(_graphicsDevice);
            grad = new Texture2D[3];
        }

        public override void LoadContent(ContentManager content)
        {
            grad[0] = _game.Content.Load<Texture2D>("Images/Leicht");
            grad[1] = _game.Content.Load<Texture2D>("Images/Mittel");
            grad[2] = _game.Content.Load<Texture2D>("Images/Schwer");
            screen = grad[0];
        }

        public override void UnloadContent()
        {
            
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState currentFrameKeyboardState = Keyboard.GetState();
            elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (currentFrameKeyboardState != lastFrameKeyboardState && (currentFrameKeyboardState.IsKeyDown(Keys.Up) || currentFrameKeyboardState.IsKeyDown(Keys.Down)))
            {
                if (currentFrameKeyboardState.IsKeyDown(Keys.Down))
                {
                    if (texture < 2)
                    {
                        texture++;
                    }
                }
                else if (currentFrameKeyboardState.IsKeyDown(Keys.Up))
                {
                    if (texture > 0)
                    {
                        texture--;
                    }
                }
            }

            screen = grad[texture];

            if (elapsedTime >= inputDelay)
            {

                if (currentFrameKeyboardState != lastFrameKeyboardState && (currentFrameKeyboardState.IsKeyDown(Keys.Enter) || currentFrameKeyboardState.IsKeyDown(Keys.Space)))
                {
                        GameState changeToMenuTestState = new CurrentGameState(_game, _graphicsDevice, _graphicsDeviceManager, texture);
                        _game.Components.Clear();
                        MazeOfRuneterra.gameStateManager.ChangeScreen(changeToMenuTestState);
                }
            }
            lastFrameKeyboardState = currentFrameKeyboardState;
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(screen,_graphicsDevice.Viewport.Bounds,Color.White);
            spriteBatch.End();
        }
    }
}
