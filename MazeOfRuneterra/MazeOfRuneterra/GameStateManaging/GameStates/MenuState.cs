﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazeOfRuneterra.Buttons;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MazeOfRuneterra.GameComponent;
using MazeOfRuneterra.Kamera;
using MazeOfRuneterra.GameStateManaging.GameStates;

namespace MazeOfRuneterra.GameStateManager.GameStates
{
    class MenuState : GameState
    {
        private SpriteBatch spriteBatch;
        private Color _backgroundColour = Color.Black;

        private float inputDelay = 0.6f;
        private float elapsedTime = 0;
        private KeyboardState lastFrameKeyboardState;

        Texture2D menuStartSelected, menuQuitSelected;
        Texture2D currentlyUsed;
        public MenuState(Game game, GraphicsDevice graphicsDevice, GraphicsDeviceManager graphicsDeviceManager) : base(game, graphicsDevice, graphicsDeviceManager)
        {
            
        }

        public override void Initialize()
        {
            
        }

        public override void LoadContent(ContentManager content)
        {
            spriteBatch = new SpriteBatch(_graphicsDevice);
            menuStartSelected = _game.Content.Load<Texture2D>("Images/MenuScreenStartSelected");
            menuQuitSelected = _game.Content.Load<Texture2D>("Images/MenuScreenQuitSelected");
            currentlyUsed = menuStartSelected;

        }

        public override void UnloadContent()
        {

        }
        
        
        public override void Update(GameTime gameTime)
        {

            KeyboardState currentFrameKeyboardState = Keyboard.GetState();
            elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
            /*
            if (currentFrameKeyboardState != lastFrameKeyboardState && currentFrameKeyboardState.IsKeyDown(Keys.R))
            {
                GameState changeToGameState = new GameStates.CurrentGameState(_game,_graphicsDevice,_graphicsDeviceManager);
                _game.Components.Clear();
                MazeOfRuneterra.gameStateManager.ChangeScreen(changeToGameState);
            }*/
            if (currentFrameKeyboardState != lastFrameKeyboardState && (currentFrameKeyboardState.IsKeyDown(Keys.Up) || currentFrameKeyboardState.IsKeyDown(Keys.Down)))
            {
                if (currentlyUsed == menuStartSelected && (currentFrameKeyboardState.IsKeyDown(Keys.Down)))
                {
                    currentlyUsed = menuQuitSelected;
                }
                else if (currentlyUsed == menuQuitSelected && (currentFrameKeyboardState.IsKeyDown(Keys.Up)))
                {
                    currentlyUsed= menuStartSelected;
                }
            }

            if (elapsedTime >= inputDelay)
            {
                
                if (currentFrameKeyboardState != lastFrameKeyboardState && (currentFrameKeyboardState.IsKeyDown(Keys.Enter) || currentFrameKeyboardState.IsKeyDown(Keys.Space)))
                {
                    if (currentlyUsed == menuStartSelected)
                    {
                        //GameState changeToGameState = new CurrentGameState(_game, _graphicsDevice, _graphicsDeviceManager);
                        GameState changeToMenuTestState = new DifficultySelectionState(_game, _graphicsDevice, _graphicsDeviceManager);
                        _game.Components.Clear();
                        MazeOfRuneterra.gameStateManager.ChangeScreen(changeToMenuTestState);
                    }
                    else
                    {
                        _game.Exit();
                    }

                }
            }
            
            

            lastFrameKeyboardState = currentFrameKeyboardState;
        }

        public override void Draw(GameTime gameTime)
        {
            _graphicsDevice.Clear(_backgroundColour);
            spriteBatch.Begin();
            spriteBatch.Draw(currentlyUsed, _graphicsDevice.Viewport.Bounds, Color.White);
            spriteBatch.End();
        }
    }
}
