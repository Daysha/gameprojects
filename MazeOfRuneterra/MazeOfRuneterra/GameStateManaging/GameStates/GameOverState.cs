﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MazeOfRuneterra.GameComponent;
using MazeOfRuneterra.Kamera;
using Microsoft.Xna.Framework.Media;

namespace MazeOfRuneterra.GameStateManager.GameStates
{
    class GameOverState : GameState
    {
        private bool gameWon = false;
        private int punkte;

        private float gebrauchteZeit = 0;
        private float elapsedTime = 0;
        SpriteBatch spriteBatch;
        private Texture2D tGameWon, tGameLost;
        private SpriteFont textFont;
        private SpriteFont otherFont;
        private string lossText = "Du hast leider verloren";
        private string winText = "Du hast gewonnen";
        private string ptText = "Punktzahl:";
        private string timeText = "Zeit:";

        private Color txtColor;
        private Color annotationColor;

        private Song endSound;
        private bool soundPlayed;
        public int Punkte
        {
            get
            {
                return punkte;
            }
            set
            {
                if (value >= 0) punkte = value;
            }
        }

        public GameOverState(Game game, GraphicsDevice graphicsDevice, GraphicsDeviceManager graphicsDeviceManager,float time, bool gameWon = false, int pkt = 0) : base(game, graphicsDevice, graphicsDeviceManager)
        {
            this.gameWon = gameWon;
            Punkte = pkt;
            gebrauchteZeit = time;
            soundPlayed = false;

            if (gameWon)
            {
                txtColor = Color.Black;
                annotationColor = Color.Green;
            }
            else
            {
                txtColor = Color.Red;
                annotationColor = Color.Violet;
            }
			
        }
        
        public override void Initialize()
        {
            spriteBatch = new SpriteBatch(_graphicsDevice);
        }

        public override void LoadContent(ContentManager content)
        {
            if(gameWon)
            {
                tGameWon = _game.Content.Load<Texture2D>("Images/GameOverScreenWon");
                endSound = _game.Content.Load<Song>("Sounds/Win_Sound");
            }
            else
            {
                tGameLost = _game.Content.Load<Texture2D>("Images/GameOverScreenLost");
                endSound = _game.Content.Load<Song>("Sounds/PoroFail");
            }

            textFont = _game.Content.Load<SpriteFont>("Font/GameOverFont");
            otherFont = _game.Content.Load<SpriteFont>("Font/TimerFont");
        }

        public override void UnloadContent()
        {

        }

        public override void Update(GameTime gameTime)
        {
            elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if(elapsedTime >=2.5f)
            {
                if (Keyboard.GetState().GetPressedKeys().Length > 0)
                {
                    if(!Keyboard.GetState().GetPressedKeys().Contains(Keys.Escape))
                    {
                        GameState changeToMenu = new MenuState(_game, _graphicsDevice, _graphicsDeviceManager);
                        _game.Components.Clear();
                        MazeOfRuneterra.gameStateManager.ChangeScreen(changeToMenu);
                    }
                    else
                    {
                        _game.Exit();
                    }
                }
            }
        }
        public override void Draw(GameTime gameTime)
        {
            if (!soundPlayed)
            {
                MediaPlayer.Play(endSound);
                soundPlayed = true;
            }
            spriteBatch.Begin();
            //Message oben
            if(gameWon)
            {
                spriteBatch.Draw(tGameWon,_graphicsDevice.Viewport.Bounds,Color.White*0.5f);
                spriteBatch.DrawString(textFont, winText, new Vector2(_graphicsDevice.Viewport.Width / 2 - (textFont.Texture.Width / 2)+40, 5), txtColor);
            }
            else
            {
                spriteBatch.Draw(tGameLost, _graphicsDevice.Viewport.Bounds, Color.White * 0.95f);
                spriteBatch.DrawString(textFont, lossText, new Vector2(_graphicsDevice.Viewport.Width / 2 - (textFont.Texture.Width/2), 30), txtColor);
            }

            //Werte
            spriteBatch.DrawString(textFont, ptText, new Vector2(_graphicsDeviceManager.PreferredBackBufferWidth / 2 - (textFont.Texture.Width / 2 * 1.75f) + 250,_graphicsDeviceManager.PreferredBackBufferHeight/5), txtColor);
            spriteBatch.DrawString(otherFont, $"{Punkte} Pkt.", new Vector2(_graphicsDeviceManager.PreferredBackBufferWidth / 2 - (textFont.Texture.Width / 2 * 1.75f) + 255, _graphicsDeviceManager.PreferredBackBufferHeight / 5 +50), Color.White);

            spriteBatch.DrawString(textFont, timeText, new Vector2(_graphicsDeviceManager.PreferredBackBufferWidth / 2 - (textFont.Texture.Width / 2 * 1.75f) + 255, (_graphicsDeviceManager.PreferredBackBufferHeight / 5) + 90), txtColor);
            spriteBatch.DrawString(otherFont, $"{Convert.ToInt32(gebrauchteZeit)}s", new Vector2(_graphicsDeviceManager.PreferredBackBufferWidth / 2 - (textFont.Texture.Width / 2 * 1.75f) + 255, (_graphicsDeviceManager.PreferredBackBufferHeight / 5) + 140), Color.White);

            //Text unten
            spriteBatch.DrawString(textFont, "Druecke irgendetwas", new Vector2(_graphicsDeviceManager.PreferredBackBufferWidth / 2 - (textFont.Texture.Width / 2 * 1.75f) + (textFont.Texture.Width/2)- 70, _graphicsDeviceManager.PreferredBackBufferHeight - (textFont.Texture.Height / 3 * 1.8f)), annotationColor);
            spriteBatch.DrawString(textFont, "um im Hauptmenu zu landen", new Vector2(_graphicsDeviceManager.PreferredBackBufferWidth / 2 - (textFont.Texture.Width / 2) - 60, _graphicsDeviceManager.PreferredBackBufferHeight - (textFont.Texture.Height / 3 * 1.2f)), annotationColor);
            spriteBatch.DrawString(textFont, "Druecke ESC um das Spiel zu beenden", new Vector2(_graphicsDeviceManager.PreferredBackBufferWidth / 2 - (textFont.Texture.Width/2*1.75f), _graphicsDeviceManager.PreferredBackBufferHeight - (textFont.Texture.Height / 3 * 0.6f)), Color.Black, 0, Vector2.Zero, 1f, SpriteEffects.None, 1);
            spriteBatch.End();

            
        }
    }
}
