﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MazeOfRuneterra.GameComponent;
using MazeOfRuneterra.GameComponents;
using MazeOfRuneterra.Kamera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Plane = MazeOfRuneterra.GameComponent.Plane;

namespace MazeOfRuneterra.GameStateManager.GameStates
{
    class CurrentGameState : GameState
    {
        private FirstPersonCamera fpCamera;
        KeyboardState lastFrameKeyboardState;
        private float elapsedTime = 0;
        private int schwierigkeitsgrad;
        private int[] punkte = { 1000,10000,100000};
        private int basepkt;
        private int basemp;
        private int[] multiplikator = {10,100,1000 };
        private int[] times = { 120,300,600};
        private int time;
        
        private Vector3 oldCameraPosition;

        VertexPositionColor[] gizmo;

        BasicEffect effect;

        MazeGenerator.MazeGenerator mazeGenerator;
        List<Box> maze;
        List<Plane> ground;

        Box start;
        Box end;

        Song background;

        public CurrentGameState(Game game, GraphicsDevice graphicsDevice, GraphicsDeviceManager graphicsDeviceManager, int grad) : base(game, graphicsDevice, graphicsDeviceManager)
        {
            fpCamera = new FirstPersonCamera(_game, new Vector3(1.5f, 0.5f, 1.5f), new Vector3(0, 0, 0));
            maze = new List<Box>();
            ground = new List<Plane>();
            oldCameraPosition = fpCamera.Position;
            schwierigkeitsgrad = grad + 1;
            basepkt = punkte[grad];
            basemp = multiplikator[grad];
            time = times[grad];
        }

        public override void Initialize()
        {
            //If aGizmo is needed
            //gizmo = new VertexPositionColor[6];
            //gizmo[0] = new VertexPositionColor(new Vector3(0, 0, 0), Color.Red);
            //gizmo[1] = new VertexPositionColor(new Vector3(10, 0, 0), Color.Red);
            //gizmo[2] = new VertexPositionColor(new Vector3(0, 0, 0), Color.Blue);
            //gizmo[3] = new VertexPositionColor(new Vector3(0, 0, 10), Color.Blue);
            //gizmo[4] = new VertexPositionColor(new Vector3(0, 0, 0), Color.Green);
            //gizmo[5] = new VertexPositionColor(new Vector3(0, 10, 0), Color.Green);
            
            mazeGenerator = new MazeGenerator.MazeGenerator(10 * schwierigkeitsgrad, 10 * schwierigkeitsgrad);
            fpCamera.Initialize();


            for (int y = 0; y < mazeGenerator.MazeData.GetLength(0); y++)
            {
                for (int x = 0; x < mazeGenerator.MazeData.GetLength(1); x++)
                {
                    ground.Add(new Plane(_game, fpCamera, new Vector3(-x * 2, 0, -y * 2), "Images/Icefloor"));
                    if (mazeGenerator.MazeData[x,y] == 1)
                    {
                        maze.Add(new Box(_game,fpCamera,new Vector3(x + 0.5f,0.5f, y + 0.5f),new Vector3(0.5f, 0.5f, 0.5f), "Images/Icewuerfel", 255f));
                    }
                    else if (mazeGenerator.MazeData[x, y] == 2)
                    {
                        start = new Box(_game, fpCamera, new Vector3(x + 0.5f, 0.5f, y + 0.5f), new Vector3(0.5f, 0.5f, 0.5f), "Images/einsamerBerg", 255f);
                        maze.Add(start);
                    }
                    else if (mazeGenerator.MazeData[x, y] == 3)
                    {
                        end = new Box(_game, fpCamera, new Vector3(x + 0.5f, 0.5f, y + 0.5f), new Vector3(0.5f, 0.5f, 0.5f), "Images/Porowuerfel", 255f);
                        maze.Add(end);
                    }
                }
            }
        }

        public override void LoadContent(ContentManager content)
        {
            background = _game.Content.Load<Song>("Sounds/Background_Sound");
            MediaPlayer.Play(background);
            _game.Components.Add(new Skybox(_game, fpCamera, new Vector3(0, 0, 0), new Vector3(30 * schwierigkeitsgrad, 30 * schwierigkeitsgrad, 30 * schwierigkeitsgrad), "Images/IceSky", 255f));

            
            foreach (Plane plane in ground)
            {
                _game.Components.Add(plane);
            }

            foreach (Box box in maze)
            {
                _game.Components.Add(box);
            }

            _game.Components.Add(new Hud(_game, fpCamera));
           
        }

        public override void UnloadContent()
        {
            
        }

        public override void Update(GameTime gameTime)
        {
            
            elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            fpCamera.Update(gameTime);
            KeyboardState currentFrameKeyboardState = Keyboard.GetState();

            if (currentFrameKeyboardState != lastFrameKeyboardState && currentFrameKeyboardState.IsKeyDown(Keys.R) )
            {
                GameState changeToGameState = new MenuState(_game, _graphicsDevice, _graphicsDeviceManager);
                _game.Components.Clear();
                MazeOfRuneterra.gameStateManager.ChangeScreen(changeToGameState);
            }

            lastFrameKeyboardState = currentFrameKeyboardState;

            //wenn Zeit abgelaufen, leite in den GameOverState über
            if(elapsedTime >= time )
            {
                GameState changeToGameOverScreen = new GameOverState(_game, _graphicsDevice, _graphicsDeviceManager, elapsedTime, false, 0);
                _game.Components.Clear();
                MazeOfRuneterra.gameStateManager.ChangeScreen(changeToGameOverScreen);
            }

            IntersectCameraBox();

           oldCameraPosition = fpCamera.Position;
        }

        public override void Draw(GameTime gameTime)
        {
            _graphicsDevice.Clear(Color.CornflowerBlue);


            effect = new BasicEffect(_game.GraphicsDevice);
            effect.World = Matrix.Identity;
            effect.View = fpCamera.View;
            effect.VertexColorEnabled = true;
            effect.Projection = fpCamera.Projection;
            effect.CurrentTechnique.Passes[0].Apply();
            //_game.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, gizmo, 0, 3);

            DepthStencilState x = _graphicsDevice.DepthStencilState;


            _graphicsDevice.DepthStencilState = DepthStencilState.None;
                      

        }

        public void IntersectCameraBox()
        {
          foreach (Box box in maze)
            {
                if (fpCamera.FpBox.Intersects(box.BoundingBox))
                {
                    if (box == end)
                    {
                        int endpunktzahl = basepkt - Convert.ToInt32(elapsedTime * basemp) + 50;
                        GameState changeToGameOverScreen = new GameOverState(_game, _graphicsDevice, _graphicsDeviceManager,elapsedTime, true, endpunktzahl);
                        _game.Components.Clear();
                        MazeOfRuneterra.gameStateManager.ChangeScreen(changeToGameOverScreen);
                    }
                    fpCamera.Position = oldCameraPosition;
                }
            }
        }
        
    }
}
