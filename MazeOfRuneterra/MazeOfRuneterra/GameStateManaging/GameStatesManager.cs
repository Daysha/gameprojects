﻿using MazeOfRuneterra.Exceptions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;

namespace MazeOfRuneterra.GameStateManager
{
    public class GameStateManager
    {
        #region Felder
        //Stack für verschiedene Spielmodi
        private Stack<GameState> _screens = new Stack<GameState>();
        private ContentManager content;
        #endregion

        #region Eigenschaften
        public ContentManager Content
        {
            set
            {
                if (value != null)
                    content = value;
            }
        }
        #endregion

        #region Methoden
        public GameStateManager()
        {

        }
        public GameStateManager(ContentManager cm)
        {
            content = cm;
        }


        public void AddScreen(GameState screen)
        {
            try
            {
                _screens.Push(screen);
                _screens.Peek().Initialize();
                if (screen.Cm != null)
                {
                    _screens.Peek().LoadContent(screen.Cm);
                }
            }
            catch (Exception e)
            {
                throw new MoRException(e, "AddScreen von SpielmodusManager");
            }
        }

        public void RemoveScreen()
        {
            if (_screens.Count > 0)
            {
                try
                {
                    var screen = _screens.Peek();
                    _screens.Pop();
                }
                catch (Exception e)
                {
                    throw new MoRException(e, "RemoveScreen von SpielmodusManager");
                }
            }
        }

        public void ClearScreens()
        {
            while (_screens.Count > 0)
            {
                _screens.Pop();
            }
        }

        public void ChangeScreen(GameState screen)
        {
            try
            {
                ClearScreens();
                AddScreen(screen);
            }
            catch (Exception e)
            {
                throw new MoRException(e, "ChangeScreen von SpielmodusManager");
            }
        }

        public void Update(GameTime gameTime)
        {
            try
            {
                if (_screens.Count > 0)
                {
                    _screens.Peek().Update(gameTime);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new MoRException(e, "Update von SpielmodusManager");
            }
        }

        public void Draw(GameTime gameTime)
        {
            try
            {
                if (_screens.Count > 0)
                {
                    _screens.Peek().Draw(gameTime);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new MoRException(e, "Draw von SpielmodusManager");
            }
        }

        public void UnloadContent()
        {
            foreach (GameState modus in _screens)
            {
                modus.UnloadContent();
            }
        }
        #endregion
    }
}
