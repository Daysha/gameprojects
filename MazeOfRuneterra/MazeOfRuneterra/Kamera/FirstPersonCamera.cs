﻿using MazeOfRuneterra.GameComponent;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;

namespace MazeOfRuneterra.Kamera
{
    public class FirstPersonCamera : BasicCamera
    {
        private Game game;
        private float speednormal = 2f;
        private float speed;
        private float rotationSpeed = MathHelper.ToRadians(150);
        private MouseState lastMouseState, centerMouseState;
        public BoundingBox FpBox;

        public new Vector3 Position
        {
            get { return base.Position; }
            set
            {
                base.Position = value;
                FpBox = new BoundingBox(new Vector3(Position.X - 0.1f, Position.Y - 0.1f, Position.Z - 0.1f), new Vector3(Position.X + 0.1f, Position.Y + 0.1f, Position.Z + 0.1f));
            }
        }

        public FirstPersonCamera(Game game, Vector3 position, Vector3 target) : base(game, position, target)
        {
            this.game = game;
			
            Position = position;
            LookAt(target);       
        }


        public void Initialize()

        {
            Mouse.SetPosition((game.GraphicsDevice.Viewport.Width / 2), (game.GraphicsDevice.Viewport.Height / 2));
            centerMouseState = Mouse.GetState();
        }


        public void Update(GameTime gameTime)
        {

            MouseState currentMouseState = Mouse.GetState();

            float x = MathHelper.ToRadians(currentMouseState.X - centerMouseState.X);
            float z = MathHelper.ToRadians(currentMouseState.Y - centerMouseState.Y);
            
                //Console.WriteLine(lastMouseState.X + " | " + lastMouseState.Y);

                Forward = Vector3.Transform(Forward, Matrix.CreateFromAxisAngle(Up, -x * rotationSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds));
                Forward = Vector3.Transform(Forward, Matrix.CreateFromAxisAngle(Right, -z * rotationSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds));
            
            //Forward = Vector3.Transform(Forward, Matrix.CreateRotationY(-x * rotationSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds));
            //Forward = Vector3.Transform(Forward, Matrix.CreateRotationZ(z * rotationSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds));
            //Forward = Vector3.Transform(Forward, Matrix.CreateFromYawPitchRoll(x,z,0));

            lastMouseState = currentMouseState;



            if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
            {
                speed = 3.5f;
            }
            else
            {
                speed = speednormal;
            }


            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {

                Position += new Vector3(Forward.X,0,Forward.Z) * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                FpBox.Min += new Vector3(Forward.X,0,Forward.Z) * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                FpBox.Max += new Vector3(Forward.X,0,Forward.Z) * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
			
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                Position -= Right * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                FpBox.Min -= Right * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                FpBox.Max -= Right * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                Position -= new Vector3(Forward.X, 0, Forward.Z) * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
				FpBox.Min -= new Vector3(Forward.X, 0, Forward.Z) * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                FpBox.Max -= new Vector3(Forward.X, 0, Forward.Z) * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            }
			
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                Position += Right * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                Console.WriteLine(Right.ToString());
                FpBox.Min += Right * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                FpBox.Max += Right * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }

            
            Mouse.SetPosition((game.GraphicsDevice.Viewport.Width / 2), (game.GraphicsDevice.Viewport.Height / 2));
        }

    }
}
