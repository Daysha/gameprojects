﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MazeOfRuneterra.Kamera
{
    public class BasicCamera : ICamera
    {
        private Matrix view = Matrix.Identity;
        private Matrix projection = Matrix.Identity;
        private float fieldOfView = MathHelper.PiOver4;
        private float aspectRatio = 1.25f;
        private float nearPlane = 0.1f;
        private float farPlane = 100f;
        private Vector3 position = Vector3.Zero;
        private Vector3 forward = Vector3.Forward;
        private Vector3 right = Vector3.Right;
        private Vector3 up = Vector3.Up;

        public float FieldOfView
        {
            get { return MathHelper.ToDegrees(fieldOfView); }
            set
            {
                if (value <= 0 || value >= 180)
                    throw new ArgumentOutOfRangeException("Fehler: das Fiel of View muss zwischen 0 und 180 Grad liegen!");
                else
                {
                    fieldOfView = MathHelper.ToRadians(value);
                    CalculateProjectionMatrix();
                }
            }
        }

        public float AspectRatio
        {
            get { return aspectRatio; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("Fehler: die AscpectRation mus größer als Null sein!");
                else
                {
                    aspectRatio = value;
                    CalculateProjectionMatrix();
                }
            }
        }

        public float NearPlane
        {
            get { return nearPlane; }
            set
            {
                if (value <= 0 || value >= farPlane)
                    throw new ArgumentOutOfRangeException("Fehler: die Near Plane muus gößer als Null und kleiner als die Far Plane sein!");
                else
                {
                    nearPlane = value;
                    CalculateProjectionMatrix();
                }
            }
        }

        public float FarPlane
        {
            get { return farPlane; }
            set
            {
                if (value <= 0 || value <= nearPlane)
                    throw new ArgumentOutOfRangeException("Fehler: die Far Plane muss gößer als die Near Plane sein!");
                else
                {
                    farPlane = value;
                    CalculateProjectionMatrix();
                }
            }
        }

        public Vector3 Position
        {
            get { return position; }
            set
            {
                position = value;
                CalculateViewMatrix();
            }
        }

        public Vector3 Forward
        {
            get { return forward; }
            set
            {
                if (value == Vector3.Zero)
                    throw new ArgumentOutOfRangeException("Fehler: der Forward Vektor muss ungleich Vector3.Zero sein!");
                else
                {
                    forward = Vector3.Normalize(value);
                    CalculateUpVector();
                    CalculateViewMatrix();
                }
            }
        }



        public Matrix View { get { return view; } }
        public Matrix Projection { get { return projection; } }

        public Vector3 Up { get { return up; } }
        public Vector3 Right { get { return right; } }

        //Matrix ICamera.View => throw new System.NotImplementedException();

        //Matrix ICamera.Projection => throw new System.NotImplementedException();

        //Vector3 ICamera.Position { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
        //Vector3 ICamera.Forward { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        //Vector3 ICamera.Right => throw new System.NotImplementedException();

        //Vector3 ICamera.Up => throw new System.NotImplementedException();

        public BasicCamera(Game game, Vector3 position, Vector3 target) 
        {

            this.position = position;
            //aspectRatio = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.AspectRatio;
            forward = Vector3.Normalize(target - position);

            CalculateUpVector();
            CalculateViewMatrix();
            CalculateProjectionMatrix();
        }

        public void CalculateProjectionMatrix()
        {
            projection = Matrix.CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearPlane, farPlane);
        }

        public void CalculateViewMatrix()
        {
            view = Matrix.CreateLookAt(position, position + forward, up);
        }

        private void CalculateUpVector()
        {
            if (forward == Vector3.Up)
            {
                right = Vector3.Right;
                up = Vector3.Backward;
            }
            else if (forward == Vector3.Down)
            {
                right = Vector3.Right;
                up = Vector3.Forward;
            }
            else
            {
                right = Vector3.Normalize(Vector3.Cross(forward, Vector3.Up));
                up = Vector3.Normalize(Vector3.Cross(right, forward));
            }
        }

        public void LookAt(Vector3 point)
        {
            Forward = Vector3.Normalize(point - position);
        }
    }
}
