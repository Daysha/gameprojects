﻿using Microsoft.Xna.Framework;

namespace MazeOfRuneterra.Kamera
{
    public interface ICamera
    {
        Matrix View { get; }
        Matrix Projection { get; }

        float FieldOfView { get; set; }

        float AspectRatio { get; set; }

        float NearPlane { get; set; }

        float FarPlane { get; set; }

        Vector3 Position { get; set; }

        Vector3 Forward { get; set; }

        Vector3 Right { get; }

        Vector3 Up { get; }

        void LookAt(Vector3 point);
    }
}
