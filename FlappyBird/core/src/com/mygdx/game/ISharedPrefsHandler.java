package com.mygdx.game;

public interface ISharedPrefsHandler {
    public static final String HIGHSCORE = "Highscore";
    public int getHighscore();
    public void setHighscore(int highscore);
}
