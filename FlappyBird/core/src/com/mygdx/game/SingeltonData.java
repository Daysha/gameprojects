package com.mygdx.game;

public class SingeltonData {
    private SingeltonData(){ }
    private static SingeltonData instance;

    public static SingeltonData getInstance() {

        if (instance == null) {
            instance = new SingeltonData(); // can only be assessed here. no other class can generate a second instance
        }
        return instance;
    }

    private ISharedPrefsHandler handler;

    public void setHandler (ISharedPrefsHandler handler){
                this.handler = handler;
            }

    public ISharedPrefsHandler getHandler () {
                return this.handler;
    }
}

