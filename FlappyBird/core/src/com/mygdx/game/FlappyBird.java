package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.ArrayList;
import java.util.Random;

public class FlappyBird extends ApplicationAdapter {

	SpriteBatch batch;
	Texture background;

	Texture[] bird;
	float birdY, birdX;
	int birdState = 0;
	int birdPause = 0;
	int coinCount = 0;
	int bombCount = 0;
	float gravitiy = 0.2f;
	float velocity = 0;
	int coinstate =0;
	int coinPause = 0;
	int bombPause = 0;
	int bombstate = 0;
	int highscore;

	ArrayList<Integer> coinXs = new ArrayList<Integer>();//x Positionen
	ArrayList<Integer> coinYs = new ArrayList<Integer>();// y Positionen
	ArrayList<Integer> bombXs = new ArrayList<Integer>();// x Position der Bomben
	ArrayList<Integer> bombYs = new ArrayList<Integer>(); // y Position der Bomben
	ArrayList<Rectangle> coinRectangles = new ArrayList<Rectangle>(); //Körper zum Überlappen
	ArrayList<Rectangle> bombRectangles = new ArrayList<Rectangle>(); //Bombenkörper
	Rectangle birdRectangle;
	Texture[] bomb;//Bild Bombe
	Texture[] coin; //Bild Münze
	Random random; //Zufallsgenerator

	private enum GAMESTATE { START, INGAME, GAMEOVER}
	private GAMESTATE gamestate;

	BitmapFont font;
	int score = 0;

	private Stage stage; //** stage holds the Button **//
	private TextureAtlas buttonsAtlas; //** image of buttons **//
	private Skin buttonSkin; //** images are used as skins of the button **//
	private TextButton button; //** the button - the only actor in program **//

	MySound music;

	@Override
	public void create () {
		music = new MySound();
		batch = new SpriteBatch();
		background = new Texture("own/bg.png");
		bird = new Texture[3];
		bird[0] = new Texture("own/flappy bird/bird_up.png");
		bird[1] = new Texture("own/flappy bird/bird_down.png");
		bird[2] = new Texture("own/flappy bird/bird_hit.png");

		birdX = Gdx.graphics.getWidth() / 2 - bird[0].getWidth() / 2;
		birdY = Gdx.graphics.getHeight() / 2 - bird[0].getHeight() / 2;

		random = new Random();
		coin = new Texture[9];
		for(int i = 0; i< coin.length; i++){
			coin[i] = new Texture("own/Coins/Coin"+(i+1)+".png");
		}

		bomb = new Texture[2];
		bomb [0] = new Texture("own/bomb.png");
		bomb[1] = new Texture("own/bomb2.png");
		gamestate = GAMESTATE.START;

		//Den Font anlegen
		font = new BitmapFont();
		font.setColor(Color.WHITE);
		font.getData().setScale(10.0f);

		highscore = SingeltonData.getInstance().getHandler().getHighscore();

		buttonsAtlas = new TextureAtlas("own/GUI/Return/ReturnButton.atlas"); //** button atlas image **//
		buttonSkin = new Skin();
		buttonSkin.addRegions(buttonsAtlas); //** skins for on and off **//

		stage = new Stage(new ScreenViewport());        //** window is stage **//
		stage.clear();
		Gdx.input.setInputProcessor(stage); //** stage is responsive **//

		TextButton.TextButtonStyle style = new TextButton.TextButtonStyle(); //** Button properties **//
		style.up = buttonSkin.getDrawable("Btn_re_Green");
		style.down = buttonSkin.getDrawable("Btn_re_white");
		style.font = font;

		button = new TextButton("",style);
		button.setHeight(500); //** Button Height **//
		button.setWidth(500); //** Button Width **//
		button.setPosition(Gdx.graphics.getWidth()/2 - button.getWidth()/2, Gdx.graphics.getHeight()/2 - button.getHeight()/2); //** Button location **//
		button.addListener(new InputListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				gamestate = GAMESTATE.START;
				stage.clear();

				if (score > highscore) {
					SingeltonData.getInstance().getHandler().setHighscore(score);
					highscore = score;
				}
				score = 0;
				gamestate = GAMESTATE.START;
				velocity = 0;
				coinXs.clear();
				coinYs.clear();
				coinRectangles.clear();
				bombXs.clear();
				bombYs.clear();
				coinCount = 0;
				bombCount = 0;
				bombRectangles.clear();
				birdY = Gdx.graphics.getHeight() / 2;
				birdState = 0;

				return true;
			}

			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
				//Gdx.app.log("my app", "Released");
			}
		});

		music.PlayMusic(0.5f);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act();

		batch.begin();

		batch.draw(background,0,0, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

		if (gamestate == GAMESTATE.START){
			if ( Gdx.input.justTouched()){
				gamestate = GAMESTATE.INGAME; //wir beginnen das Spiel
			}
		}
		else if(gamestate == GAMESTATE.INGAME) {
			if (coinCount < 100) { //alle 100 Millisekunden soll eine Münze erscheinen
				coinCount++;
			} else {
				coinCount = 0; //wir fangen neu an zu zählen und erstellen eine Münze
				createCoin();
			}
			if (bombCount < 250) {
				bombCount++;
			} else {
				bombCount = 0;
				createBomb();
			}
		}

		coinRectangles.clear(); // alle Coin-Körper löschen
		bombRectangles.clear();

		switch (gamestate) {

			case INGAME:
				//Münzen bewegen

				if(coinPause >= 7) {
					coinPause = 0;
					if (coinstate < coin.length - 1) {
						coinstate++;
					} else {
						coinstate = 0;
					}
				}
				else{
					coinPause++;
				}

				if (coinXs.size() > 0) { // wenn es Münzen gibt.

					//für alle Münzen in der Arraylist
					for (int i = 0; i < coinXs.size(); i++) {
						batch.draw(coin[coinstate], coinXs.get(i), coinYs.get(i));
						//Münze um 4 Pixel nach links verschieben
						coinXs.set(i, coinXs.get(i) - 4);

						//die Rectangles als Körper zur Collision Detection ermitteln
						coinRectangles.add(new Rectangle(coinXs.get(i), coinYs.get(i), coin[0].getWidth(), coin[0].getHeight()));
					}
				}

				//Bomben bewegen
				if(bombPause >= 7) {
					bombPause = 0;
					if (bombstate < bomb.length - 1) {
						bombstate++;
					} else {
						bombstate = 0;
					}
				}
				else{
					bombPause++;
				}

				for (int i = 0; i < bombXs.size(); i++) {
					batch.draw(bomb[bombstate], bombXs.get(i), bombYs.get(i));
					//Münze um 4 Pixel nach links verschieben
					bombXs.set(i, bombXs.get(i) - 4);

					//die Rectangles als Körper zur Collision Detection ermitteln
					bombRectangles.add(new Rectangle(bombXs.get(i), bombYs.get(i), bomb[0].getWidth(), bomb[0].getHeight()));
				}

				if (birdPause < 7) {
					birdPause++;
				} else {
					birdPause = 0;
					if (birdState < bird.length - 2) {
						birdState++;
					} else {
						birdState = 0;
					}
				}

				if (Gdx.input.justTouched()) {
					velocity -= 10;
				}

				velocity += gravitiy;
				birdY -= velocity;

				if (birdY > Gdx.graphics.getHeight() - bird[birdState].getHeight()) {
					birdY = Gdx.graphics.getHeight() - bird[birdState].getHeight();
					velocity = 0;
				}

				if (birdY <= 0) {
					birdY = 0;
				}

				/**  * Zur Kollisionserkennung gibt es sogenannte Rectangles, deren Überlappung geprüft werden kann  */
				birdRectangle = new Rectangle(birdX, birdY, bird[birdState].getWidth(), bird[birdState].getWidth());

				/**  * Prüfung, ob der Man mit einer anderen Figur kollidiert.  */


				for (int i = 0; i < coinRectangles.size(); i++) {
					if (Intersector.overlaps(birdRectangle, coinRectangles.get(i))) {
						Gdx.app.log("game", "coin");
						score++;
						coinRectangles.remove(i);
						coinXs.remove(i);
						coinYs.remove(i);
						i = coinRectangles.size();
					}
				}

				for (int i = 0; i < bombRectangles.size(); i++) {
					if (Intersector.overlaps(birdRectangle, bombRectangles.get(i))) {
						Gdx.app.log("game", "bombe");
						gamestate = GAMESTATE.GAMEOVER; //Spiel ist beendet
						bombRectangles.remove(i);
						bombXs.remove(i);
						bombYs.remove(i);
						i = bombRectangles.size();
					}
				}
				break;

			case GAMEOVER:
				birdY -= 20;
				if (birdPause < 7) { //Nur bei jedem 8. Frame machen wir etwas
					birdPause++;
				} else {
					birdPause = 0;
				}
				birdState = 2;

				/*	if (manState == 5) {
						//die Zustände 5 und 6 sind die dizzy Bilder
						// manState = 6;
					}
					else {
					  manState = 5;
					 }
				 */
				// Neustart des Spiels
				if(birdY < 0) {
					stage.addActor(button);
					stage.getBatch().begin();
					stage.getBatch().draw(background,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
					stage.getBatch().end();
					stage.draw();
				}

				break;

			default: ;
		}

			batch.draw(bird[birdState], birdX, birdY);
			//Den Score anzeigen (am besten, bevor der batch geschlossen wird)
			// Hier an der x-Position 100 und der y-Position 200 //Score ausgeben
			font.draw(batch, "Score: " + String.valueOf(score), 100, 200);
			font.draw(batch,"HighScore: "+String.valueOf(highscore),100,Gdx.graphics.getHeight()-font.getCapHeight());

		batch.end();
	}

	
	@Override
	public void dispose () {
		batch.dispose();
		// resourcen wieder freigeben
		for(int i = 0; i < bird.length; i++){
			bird[i].dispose();
		}

		for(int i = 0; i < coin.length; i++){
			coin[i].dispose();
		}
		for(int i = 0; i < bomb.length; i++){
			bomb[i].dispose();
		}
		buttonSkin.dispose();
		buttonsAtlas.dispose();

		font.dispose();
		stage.dispose();
		music.DestroyAudio();
	}

	public void createCoin(){
		float height = random.nextFloat()*Gdx.graphics.getHeight();
		if(height-coin[0].getHeight() < 0 ){
			height = 0+coin[0].getHeight();
		}
		if(height+coin[0].getHeight() > Gdx.graphics.getHeight()){
			height = Gdx.graphics.getHeight() - coin[0].getHeight();
		}
		coinYs.add((int) height);
		coinXs.add(Gdx.graphics.getWidth());
	}

	public void createBomb(){
		float height = random.nextFloat() * Gdx.graphics.getHeight();

		if(height-bomb[0].getHeight() < 0 ){
			height = 0+bomb[0].getHeight();
		}
		if(height+bomb[0].getHeight() > Gdx.graphics.getHeight()){
			height = Gdx.graphics.getHeight() - bomb[0].getHeight();
		}

		bombYs.add((int) height);
		bombXs.add(Gdx.graphics.getWidth());
	}
}
