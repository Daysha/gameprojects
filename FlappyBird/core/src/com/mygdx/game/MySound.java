package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.audio.Music;

public class MySound {
    private static Sound sound = (Sound) Gdx.audio.newSound(Gdx.files.internal("own/music.mp3"));

    public static void PlayMusic(float volume){
        sound.setLooping(1,true);
        sound.setVolume(1,volume);
        sound.play();
    }


    public static void DestroyAudio(){
        sound.dispose();
    }

}
