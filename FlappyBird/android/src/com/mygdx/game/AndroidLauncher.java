package com.mygdx.game;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.mygdx.game.FlappyBird;

public class AndroidLauncher extends AndroidApplication implements ISharedPrefsHandler {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new FlappyBird(), config);
		SingeltonData.getInstance().setHandler(this);
	}

	@Override
	public int getHighscore() {
		SharedPreferences preferences = getSharedPreferences(ISharedPrefsHandler.HIGHSCORE,MODE_PRIVATE);
		return preferences.getInt(ISharedPrefsHandler.HIGHSCORE,0);
	}

	@Override
	public void setHighscore(int highscore) {
		SharedPreferences preferences = getSharedPreferences(ISharedPrefsHandler.HIGHSCORE,MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(ISharedPrefsHandler.HIGHSCORE,highscore);
		editor.apply();
	}
}
