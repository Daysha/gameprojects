﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movementVR : MonoBehaviour {

    // Use this for initialization
    public GameObject bucketMother;
    public string movementLoR;
    public float Speed;
    public float MaxMove;
    public float MinMove;

    private string MovementLoR
    {
        get { return movementLoR; }
        set { if (value.ToLower() == "right" ||  value.ToLower() == "left")
                {
                    movementLoR = value;
                } }
    }

    private int movement;
    private float move;
    private bool isLookAt;

	void Start () {

        movement = 0;
	}
	
	// Update is called once per frame
	void Update () {

        if (isLookAt)
        {
            move += movement * Speed * Time.deltaTime;

            Debug.Log("" + movement + " " + Speed + " " + Time.deltaTime + " " + move);

            move = Mathf.Clamp(move, MinMove, MaxMove);

            bucketMother.transform.position = new Vector3(bucketMother.transform.position.x, bucketMother.transform.position.y, move);
        }
        else {
            move = bucketMother.transform.position.z;
        }
    }

    public void PointerEnter()
    {
        switch (MovementLoR.ToLower())
        {
            case "right":  movement = 1;break;
            case "left": movement = -1;break;
        }
        isLookAt = true;
    }

    public void PointerExit()
    {
        isLookAt = false;
        
    }
}
