﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startGame : MonoBehaviour {

    public GameObject Ball;
    public float TimeToClick;
    private float timer;
    private bool pointerOn;
	// Use this for initialization
	void Start () {
        timer = 0;
        pointerOn = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (pointerOn)
        {
            timer += Time.deltaTime;
            if (timer >= TimeToClick)
            {
                StartGame();
            }
        }
	}

    public void PointerEnter()
    {
        pointerOn = true;
    }

    public void PointerExit()
    {
        pointerOn = false;
    }

    public void StartGame()
    {
        Debug.Log("Click");
        Ball.SetActive(true);
    }
}
