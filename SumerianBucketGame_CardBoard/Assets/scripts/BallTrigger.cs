﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallTrigger : MonoBehaviour {

    // Use this for initialization

    public GameObject bucked1, bucked2, desk, counter;
    public int trigger;
    public float rotation;
    public Material material1, material2;


    private Vector3 startPosition;
    private int counterDesk, counterGame;

	void Start () {
        startPosition = transform.position;
        counterDesk = 0;
        counterGame = 0;
        ChangeMaterial();
    }
	
	// Update is called once per frame
	void Update () {


        if (GetComponent<Collider>().bounds.Intersects(bucked1.GetComponents<Collider>()[0].bounds) ||
            GetComponent<Collider>().bounds.Intersects(bucked2.GetComponents<Collider>()[0].bounds))
        {

            if (GetComponent<MeshRenderer>().material.name == checkBucked().GetComponentInChildren<MeshRenderer>().material.name)
            {
                counterGame++;
                counter.GetComponent<Text>().text = counterGame.ToString();
                Debug.Log(counterGame);
            }

            transform.position = startPosition;
            counterDesk++;
            ChangeMaterial();

            if(counterDesk%trigger == 0)
            {
                desk.transform.Rotate(desk.transform.rotation.x, desk.transform.rotation.y, desk.transform.rotation.z-rotation);
            }

            

        }

	}


    private void ChangeMaterial() {
        int rndMaterial = Random.Range(0,2);
        
        switch (rndMaterial)
        {
            case 0: GetComponent<MeshRenderer>().material = material1;break;
            case 1: GetComponent<MeshRenderer>().material = material2;break;
        }
    }

    private GameObject checkBucked()
    {
        if (GetComponent<Collider>().bounds.Intersects(bucked1.GetComponent<Collider>().bounds)){
            return bucked1;
        }
        if (GetComponent<Collider>().bounds.Intersects(bucked2.GetComponent<Collider>().bounds)){
            return bucked2;
        }
        else throw new System.Exception("Kein Bucked getroffen");
    }

}
