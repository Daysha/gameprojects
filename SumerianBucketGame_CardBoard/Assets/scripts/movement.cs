﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour {

    // Use this for initialization

    public float MaxMove;
    public float MinMove;
    public float Speed;

    private float move;
    private float axis;
       

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        axis = Input.GetAxis("Horizontal");

        move += axis * Speed * Time.deltaTime;
        move = Mathf.Clamp(move, MinMove, MaxMove);

        transform.position = new Vector3(transform.position.x, transform.position.y, move);

    }
}
