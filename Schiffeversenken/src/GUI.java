import java.awt.GraphicsConfiguration;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.*;

public class GUI extends JFrame {
  
  private SpielfeldGui spielFG;
  private JLabel counter;
  private JLabel gewonnen, verloren, jtitel;
  private JPanel pGewonnenoVerloren,kopf;
  private int counter_stand = 60; // Z�hlstand f�r die Z�ge
  private int counter_schiff = 0; // Z�hlstand wieviele Schiffsteile schon getroffen wurden
  
  
  /*********** Construktoren ******************/	

  public GUI(String titel) {
	  
    super(titel);
    
    //Aufbau des Fensters
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setResizable(false);
    setSize(256, 350);
    
    
    spielFG = new SpielfeldGui("Schiffeversenken", 10);
    
    jtitel = new JLabel("Schiffeversenken \n");
    jtitel.setFont(new Font("TimesRoman", Font.PLAIN, 25));
    
    counter = new JLabel("Verbleibende Z�ge: " + counter_stand);
    gewonnen = new JLabel("Du hast gewonnen!");
    verloren = new JLabel("Du hast verloren!");
    
    gewonnen.setVisible(false);
    verloren.setVisible(false);

    //erstellen des Panels f�r die gewonnen und Verlorenanzeige
    pGewonnenoVerloren = new JPanel();   
    pGewonnenoVerloren.add(verloren);
    pGewonnenoVerloren.add(gewonnen);   
    pGewonnenoVerloren.setVisible(false);
    
    kopf = new JPanel();
    kopf.setLayout(new BorderLayout());
    kopf.add(jtitel,BorderLayout.NORTH);
    kopf.add(counter, BorderLayout.CENTER);
    
    //aufbau des Fensters
    add(pGewonnenoVerloren, BorderLayout.SOUTH);    
    add(spielFG, BorderLayout.CENTER);
    add(kopf,BorderLayout.NORTH);
  }

  
 /*************Methoden*************/ 
  
  /**javadoc
   * Z�hlt den Counter runter solange bis er null erreicht. 
   *
   */
  
  public void counterRunter() {
	  
    if (checkzuege()) {
      counter_stand--;
      counter.setText("Verbleibende Z�ge: " + counter_stand);
    }    
  }

/** Javadoc
 * checkt ob man Verloren hat. Wenn man verloren hat, kommt die entsprechende Meldung.
 */
public void checkVerloren() {
	  if (!checkzuege() && !counter_shiffeHoch()) {
	      pGewonnenoVerloren.setVisible(true);
	      verloren.setVisible(true);	      
	    }
  }
  
  /** Javadoc
   * checkt ob noch Z�ge durchgef�hrt werden k�nnen
   * @return true = es k�nnen noch Z�ge durchgef�hrt werden, false = es k�nnen keine Z�ge mehr durchgef�hrt werden.
   */
  
  public boolean checkzuege() {
 
	  return counter_stand>0;
  }
  
  /** Javadoc
   * Z�hlt die getroffen schiffe und setze den Counter damit hoch, wenn alle Schiffsteile getroffen wurden wird Gewonnen eingeblendet
   *  * @return: Ob man bereits gewonnen hat. True = Gewonnen.
   */

  	public boolean counter_shiffeHoch() {
  		
  		if (counter_schiff < 29) {
  			counter_schiff++;

  		} else {
  			pGewonnenoVerloren.setVisible(true);
			gewonnen.setVisible(true);
  			counter_stand = 0;
  			return true;
  		}
  		
  		return false;
  	}

  	
/*****Setter und Getter **************************************/
  	
  public SpielfeldGui getSpielFG() {
    return spielFG;
  }

  public void setSpielFG(SpielfeldGui s) {
    this.spielFG = s;
  }

  public int getCounter_stand() {
    return counter_stand;
  }

  public void setCounter_stand(int counter_stand) {
    this.counter_stand = counter_stand;
  }
}

