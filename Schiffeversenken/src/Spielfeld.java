import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class Spielfeld extends JButton {

	private boolean schon_gefaerbt; //check ob dieses Feld schon getroffen wurde
	private int number; //Nummerierung der Felder
	private String feldtyp; // Typ (Schiff oder Wasser)
	

/********************* Construktor ***********************/

	public Spielfeld(int fNummer) {
		super();
		
		setPreferredSize(new Dimension(25,25));
		setFocusPainted(false);
		setContentAreaFilled(false);
		setOpaque(true);
		setBorder(new LineBorder(Color.BLACK));
		feldtyp = "Wasser";
		number = fNummer;
		schon_gefaerbt = false;
	
	}
	
/***********************Methoden*************************/
	
	/**javadoc
	 * pr�fung des Types und setzung der farbe.
	 */

	
	public void farbewechseln() {
		
		switch (feldtyp) {	
				case "Wasser":
					if (!schon_gefaerbt) {
						setBackground(Color.BLUE);

						schon_gefaerbt = true;
					}
					break;
					
				case "Schiff":
					if (!schon_gefaerbt) {
						setBackground(Color.RED);

						schon_gefaerbt = true;
					}
					break;
	    }
		
	}
	
/**javadoc
 * Pr�fung ob dieses Feld Wasser ist oder ein Schiff.
 * @return true wenn Wasser, false wenn Schiff
 */
	
	public boolean isFeldWasser() {
		
		return feldtyp=="Wasser";
	}



		
/*****Setter und Getter **************************************/
	

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getFeldtyp() {
		return feldtyp;
	}

	public void setFeldtyp(String feldtyp) {
		this.feldtyp = feldtyp;
	}
	
	 public boolean isSchon_gefaerbt() {
		    return schon_gefaerbt;
		  }

	public void setSchon_gefaerbt(boolean schon_gefaerbt) {
		    this.schon_gefaerbt = schon_gefaerbt;
		  }

}
