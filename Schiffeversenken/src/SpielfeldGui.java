import java.awt.Color;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class SpielfeldGui extends JPanel {
	
private Spielfeld[] feld; // Array der Spielfelder
private Schiff[] schiffe; // Schiffe Array
private int spielfeldGro�e; // Gr��e des Spielfeldes

private int fNummer = 0; //Nummerierung der Felder
private Random rnd;



/*********** Construktoren ******************/	
	public SpielfeldGui(String name, int spielfeldGroe�e ) {
		super();
		
		this.spielfeldGro�e = spielfeldGroe�e * spielfeldGroe�e;		
		feld = new Spielfeld[spielfeldGro�e];
		rnd = new Random();
		
		//erstelle Spielfeld
		erstellen();

		setSize(275,300);
		setLayout(new FlowLayout(5,0,0));
		
		schiffe = new Schiff[10];
		
		//setze Schlachtschiff
		setzeSchiffe(9,5,0);
				
		//setze Kreuzer
		setzeSchiffe(7,4,0);
		setzeSchiffe(8,4,0);
		
		//setze Zerst�rer
		setzeSchiffe(4,3,0);
		setzeSchiffe(5,3,0);
		setzeSchiffe(6,3,0);
		
		//Setze Uboote
		setzeSchiffe(0,2,0);
		setzeSchiffe(1,2,0);
		setzeSchiffe(2,2,0);
		setzeSchiffe(3,2,0);	
				
	}
	
/*************Methoden*************/
	
	/**javadoc
	 * Erstellen der Spielfelder ( 10 x 10)
	 */
	public void erstellen() {
		
		for(int z = 0; z<feld.length; z++) {
				
			feld[z] = new Spielfeld(fNummer);
			
			add(feld[z]);
	
			fNummer++;
			}
		}
	
/** javadoc
 * Methode zum setzen der Schiffe auf dem Spielfeld.
 * 	+ Pr�fung ob sich das Schiff am Rande des Spielfeldes befindet
 *  + Pr�fung ob das Schiff auf einem Schiff Plaziert wird
 *  + Pr�fung ob ein schiff direkt daneben ist
 */
	
	public void setzeSchiffe(int schiffArrayPosition,int schiffsgroe�e, int position) {
		
		int schiffGebaut = 0;
		if(position == 0) { position = rnd.nextInt(100);}
			
		
		schiffe[schiffArrayPosition] = new Schiff(schiffsgroe�e,position-1);
		Schiff s = schiffe[schiffArrayPosition];
		
		
					do {

						if(s.checkSpielrand(10,99) && checkNaheFelder(s)) {

							for (int i =0; i<s.getSchiff().length; i++) {
								
								if(feld[s.getSchiff()[i]].isFeldWasser() ) {
									feld[s.getSchiff()[i]].setFeldtyp("Schiff");
									//feld[s.getSchiff()[i]].farbewechseln(); //zum testen ob die schiffe richtig gesetzt werden
									schiffGebaut++;
									}
								else break;
							}
						}
	
						if(schiffGebaut<s.getSchiff().length) {
							position = rnd.nextInt(100);
							s = new  Schiff(schiffsgroe�e,position-1);
							schiffGebaut = 0;
							}
						//System.out.println(s);
						//System.out.println(""+schiffGebaut); //zum checken wo er die schiffe setzt
					}
					while (schiffGebaut<s.getSchiff().length);
					
			schiffe[schiffArrayPosition] = s;	
		}
			
/**Javadoc
 * Checkt ob sich das zu setzende Schiff direkt neben einem anderem Schiff platziert wird.
 * 
 * @param s �bergabe des Schiffes welches plaziert werden soll.
 * @return True es ist kein Schiff in der n�he, und false es ist ein schiff in der n�he.
 */
	public boolean checkNaheFelder(Schiff s) {
		
		//k f�r Kopie
		Schiff kRechts = s.erstelleKopie(1);
		Schiff kLinks = s.erstelleKopie(-1);
		Schiff kOben = s.erstelleKopie(-10);
		Schiff kUnten = s.erstelleKopie(+10);
		
		
		//c f�r Check, r f�r richtung,
		boolean cRechts,cLinks,cOben,cUnten,waagerecht,rcheck,lcheck,ucheck,ocheck;
		
		rcheck = true;
		ocheck = true;
		ucheck = true;
		lcheck = true;
		
		waagerecht = s.istWaagerecht();
		
		
		
		if(waagerecht) {
			cRechts = kRechts.getSchiff()[kRechts.getSchiff().length-1]%10 != 0 && kRechts.getSchiff()[kRechts.getSchiff().length-1]<=99;
			cLinks = kLinks.getSchiff()[0]%9 > 0;
			cUnten = kUnten.checkSpielrand(10, 99);
			cOben = kOben.checkSpielrand(10, 99);
			
			
			if(cRechts) {rcheck = feld[kRechts.getSchiff()[kRechts.getSchiff().length-1]].isFeldWasser();}
			if(cLinks) 	{lcheck = feld[kLinks.getSchiff()[0]].isFeldWasser();}
			if(cUnten) { 
				for (int i =0; i<kUnten.getSchiff().length; i++) {
					ucheck = feld[kUnten.getSchiff()[i]].isFeldWasser();
					if (!ucheck) i = kUnten.getSchiff().length;
				}
			}
			if(cOben) { 
				for (int i =0; i<kOben.getSchiff().length; i++) {
					ocheck = feld[kOben.getSchiff()[i]].isFeldWasser();
					if (!ocheck) i = kOben.getSchiff().length;
					}
			}				
		}
		else  {
			cOben = kOben.getSchiff()[0] >= 0;
			cUnten = kUnten.getSchiff()[kUnten.getSchiff().length-1] < 99;
			cRechts = kRechts.checkSpielrand(10, 99);
			cLinks = kLinks.checkSpielrand(10, 99);

			
			if(cOben) {ocheck = feld[kOben.getSchiff()[0]].isFeldWasser();}
			if(cUnten) 	{ucheck = feld[kUnten.getSchiff()[kUnten.getSchiff().length-1]].isFeldWasser();}
			if(cRechts) { 
				for (int i =0; i<kRechts.getSchiff().length; i++) {
					rcheck = feld[kRechts.getSchiff()[i]].isFeldWasser();
					if (!rcheck) i = kRechts.getSchiff().length;
				}
			}
			if(cLinks) { 
				for (int i =0; i<kLinks.getSchiff().length; i++) {
					lcheck = feld[kLinks.getSchiff()[i]].isFeldWasser();
					if (!lcheck) i = kLinks.getSchiff().length;
					}
			}				
		}		

		return rcheck && ucheck && lcheck && ocheck;
	}
		

/*****Setter und Getter **************************************/
	
		public void setFeld(Spielfeld[] feld) {
			this.feld = feld;
		}

		public Schiff[] getSchiffe() {
			return schiffe;
		}

		public void setSchiffe(Schiff[] schiffe) {
			this.schiffe = schiffe;
		}
		
		public Spielfeld[] getFeld() {
			return feld;
		}
}
