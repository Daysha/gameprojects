import java.util.Random;

public class Schiff {
	
	private int[] schiff; //Welche Nummer die Schifssteile haben
	private int pos; //Anfang des Schiffes
	private Random rnd;
	
	// Seiten
	private int rechts,unten; // in welcher richtung das Schiff gebaut werden soll, nach rechts oder nach Links

//***** Konstruktoren **************************//
	public Schiff(int anzKasten) {
		

		rechts = 1;
		unten = 10;
		rnd = new Random();	
		schiff = new int[anzKasten];				
		
	}

	// Konstruktor zum testen
		public Schiff(int anzKasten, int pos) {
		
		this(anzKasten);
		this.setPos(pos);
		bauen();
		
	}
		
		public Schiff() {
			
		}

		
/**************** Methoden **********/
		
/* javadoc
 * Bauen der Schiffe entsprechend der K�stchenanzahl ( Uboot 2 K�stchen gro�)
 */
		
	public void bauen() {
		
		int richtung = richtung();
		int bauer = richtung;

		schiff[0] = pos;
		
		for ( int i = 1; i < schiff.length; i++) {
			schiff[i] = schiff[0]+bauer;
			bauer += richtung;
		}

	}
	
/* javadoc
 * Zuf�llig bestimmen in welcher Richtung das Schiff gebaut werden soll, Horizontal oder Vertikal.
 */
	
	public int richtung() {
		
		int zufall = rnd.nextInt(2)+1;
		
		if(zufall == 1) {
			return rechts;
		}
		else return unten;
		
	}
	
	

	/** javdoc
	 *  Checken ob das Schiff am Rand des Spielfeldes gesetzt wird.
	 * @param eingabe der SpielfeldGUI Parameter (gr��e des Spielfeldes.
	 * @return True oder false je nachdem ob das Schiff �ber den rand l�uft.
	 */
		public boolean checkSpielrand(int rechtesEnde, int MaxFelder) {
			
			boolean check = false;
			
				for (int i =0; i<schiff.length; i++) {
									
					if((schiff[i])%rechtesEnde != 0 && schiff[i]<=MaxFelder && schiff[i]>=0) {

						check = true;
						}
					else {check = false; i=schiff.length;}					
				}
				
				if(schiff[0]%rechtesEnde == 0 && schiff[0] !=-10 && !check && schiff[0]<=MaxFelder && schiff[schiff.length-1]<=MaxFelder) {
					check = true;
					}
				
				return check;		
		}
	/** javadoc
	 * Erstellen einer Kopie des Schiffes	
	 * @param position in der das Neue Schiff gebaut werden soll.
	 * @returnEine Kopie des Schiffes an einer anderen Position
	 */
		public Schiff erstelleKopie(int position) {
			
			Schiff s = new Schiff(schiff.length);
			int[] itemp = new int[schiff.length];
			
			for ( int i=0; i<schiff.length; i++) {				
				itemp[i] = schiff[i]+position;				
			}
			s.setSchiff(itemp);
			
			return s;
		}
	
		public boolean istWaagerecht() {
			
			return schiff[1] == schiff[0]+1;
		}

		
		
/** javadoc
 * toString methode f�rs Schiff um die Zahlen im Int Array zur�ckzugeben.
 */
		@Override
		public String toString() {
			String temp ="";
			
			for (int s : schiff) {
				temp+= " "+s;
			}
			
			return temp;
		}	

		
//************* getter und Setter *********************//

	public int getPos() {
		return pos;
	}



	public void setPos(int pos) {
		if(pos<100) {
		this.pos = pos;
		}
		else this.pos = 99;
	}


	public int[] getSchiff() {
		return schiff;
	}

	public void setSchiff(int[] schiff) {
		this.schiff = schiff;
		pos = schiff[0];
	}
		
	
}
