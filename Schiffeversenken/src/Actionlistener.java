import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Actionlistener implements ActionListener {
	
	

	private static SpielfeldGui sGui;
	private GUI gui;

/******************************** Construktoren *********************************/
	public Actionlistener(GUI g) {

		this.sGui = g.getSpielFG();
		this.gui = g;
		for (int z = 0; z < 100; z++) {

			sGui.getFeld()[z].addActionListener(this);

		}
	}

/*****************Methoden **********************/
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	
			if (!((Spielfeld) e.getSource()).isSchon_gefaerbt() && gui.checkzuege()) {
				gui.counterRunter();
				((Spielfeld) e.getSource()).farbewechseln();
				if (!((Spielfeld) e.getSource()).isFeldWasser()) {
					gui.counter_shiffeHoch();
				}
				gui.checkVerloren();
			}
			
		}	
}
