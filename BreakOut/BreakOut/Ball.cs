﻿using Bib.Bg.Xna2D;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakOut
{
    public class Ball : BasicSpriteComponent
    {
        private float speed;
        private Vector2 richtung;
        private float startspeed;
        private Random rnd = new Random();
        private int counterZiegelstein;
        private int framecounter;
        private int maxFrame;
        private int maxspeed;
        
        public Ball(Game game, String name, String imageName) : base(game, name, imageName)
        {
            startspeed = 30;
            counterZiegelstein = 0;
            framecounter = 0;
            maxFrame = 20;
            maxspeed = 60;
        }

        public override void Update(GameTime gameTime)
        {
            /**** Bewegenung des Balls ****/
            framecounter++;
            Position += richtung * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            /***** Wenn der Ball oben ankommt soll er nach unten reflektiert werden*******/
            if(Position.Y <= 0) {
                richtung = Vector2.Reflect(richtung, Vector2.UnitY);
            }

            base.Update(gameTime);
        }

        /******** Kollisions erkennung allgemein *******/
        public void Collision(IBasicSprite gameObjekt)
        { 

            if (gameObjekt.GetType() == typeof(Plattform))
            {
                CollisionPlattform(((Plattform)gameObjekt));
            }

            if (gameObjekt.GetType() == typeof(Ziegelstein))
            {
                CollisionZiegelstein((Ziegelstein)gameObjekt);
             }

            if (gameObjekt.Name == "Wand Links" || gameObjekt.Name == "Wand Rechts")
             {
                 CollisionWand((BasicSpriteComponent)gameObjekt);
             }
            
        }

  /**** Kollisions erkennung beim paddle *******/      
        private void CollisionPlattform(Plattform plattform)
        {
            if (Position.Y + Size.Y >= plattform.Bounds.Top && framecounter >= maxFrame)
            {
                framecounter = 0;

                if (Position.X < plattform.Position.X + plattform.TextureSize.Y / 3)
                {
                    richtung = Vector2.Reflect(richtung, new Vector2(0.196f, 0.981f));
                    Console.WriteLine("Links");
                }
                else if (Position.X > plattform.Position.X + ((plattform.Texture.Width / 3) * 2) && Position.X < plattform.Position.X + plattform.Texture.Width)
                {
                    richtung = Vector2.Reflect(richtung, new Vector2(-0.196f, 0.981f));
                    Console.WriteLine("Rechts");
                }
                else
                {
                    richtung = Vector2.Reflect(richtung, Vector2.UnitY);
                    Console.WriteLine("Mitte");
                }

            }
        }
 /**** Kollisions erkennung beim Ziegelstein*******/
        private void CollisionZiegelstein(Ziegelstein ziegelstein)
        {
            counterZiegelstein++;
            if (counterZiegelstein % 4 == 0 && speed <maxspeed)
            {
                speed += 5;
            }

            if(Position.Y < ziegelstein.Position.Y || Position.Y+ Size.Y > ziegelstein.Position.Y + ziegelstein.Texture.Height)
            {
                richtung = Vector2.Reflect(richtung, Vector2.UnitY);
                
            }
            else
            {
                richtung = Vector2.Reflect(richtung, Vector2.UnitX);
               
            }
        }

 /**** Kollisions erkennung bei Wänden Links und rechts*******/
        public void CollisionWand(BasicSpriteComponent wand)
        {
            richtung = Vector2.Reflect(richtung, Vector2.UnitX);
        }

 /**** setzen des Balls in das Spiel ***/
        public void BallReset()
        {
            Position = new Vector2(rnd.Next(60, GraphicsDevice.Viewport.Width - 60),rnd.Next(340,GraphicsDevice.Viewport.Height/2));
            richtung = new Vector2(rnd.Next(-10, 10), rnd.Next(5, 10));
            speed = startspeed;
        }

 /**** Prüfen ob der Ball ausserhalb/Unterhalb des Spielfelds ist ***/
        public bool Ballout()
        {
            if (Position.Y > GraphicsDevice.Viewport.Width)
            {
                return true;
            }
            else return false;
        }

    }

}
