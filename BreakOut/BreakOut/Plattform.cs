﻿using Bib.Bg.Xna2D;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakOut
{
    public class Plattform : BasicSpriteComponent
    {
        private float speed;
        private KeyboardState keyState;

        public Plattform(Game game, String name, String imageName) : base(game, name, imageName)
        {
            speed = 500;           
            keyState = Keyboard.GetState();
        }

        public override void Update(GameTime gameTime)
        {
            keyState = Keyboard.GetState();
/*** Bewegung des Paddels mit der Linkten und rechten Pfeiltaste ****/
            if (keyState.IsKeyDown(Keys.Left) && Position.X >30){
                Position -= Vector2.UnitX * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            }
            if (keyState.IsKeyDown(Keys.Right) && Position.X < GraphicsDevice.Viewport.Width-Texture.Width - 40)
            {
                Position += Vector2.UnitX * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;             
            }

            base.Update(gameTime);
        }
    }
}
