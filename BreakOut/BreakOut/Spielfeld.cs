﻿using Bib.Bg.Xna2D;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace BreakOut
{
    /// <summary>
    /// This is the main type for your game.
    /// 
    /// </summary>
    /// 
    public class Spielfeld : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public List<Ziegelstein> ZiegelsteinWand;
        private int anzahlZiegelsteineimSpiel;
        public Ball Ball;
        public Plattform Paddle;
        public BasicSpriteComponent WandLinks;
        public BasicSpriteComponent WandRechts;
        private int punktestand;
        private int leben;
        private SpriteFont anzeige;
        private BasicSpriteComponent gameOverAnzeige;
        private BasicSpriteComponent gewonnenAnzeige;

        public Spielfeld()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            anzahlZiegelsteineimSpiel = 112;
            punktestand = 0;
            leben = 3;

            graphics.PreferredBackBufferWidth = 1020;
            graphics.PreferredBackBufferHeight = 980;

            graphics.IsFullScreen = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            /******** Erstellung der Spielwände Links und Rechts ******************/
            WandLinks = new BasicSpriteComponent(this, "Wand Links", "Bilder\\Wall");
            WandRechts = new BasicSpriteComponent(this, "Wand Rechts", "Bilder\\Wall");

            WandLinks.Destination = new Rectangle(20, 0, 20, GraphicsDevice.Viewport.Height);
            WandRechts.Destination = new Rectangle(GraphicsDevice.Viewport.Width-40, 0, 20, GraphicsDevice.Viewport.Height);

            Components.Add(WandRechts);
            Components.Add(WandLinks);

            /******** Erstellen der Plattform ************/
            Paddle = new Plattform(this, "Plattform", "Bilder\\Paddle");
            Paddle.Destination = new Rectangle(GraphicsDevice.Viewport.Width / 2 - Paddle.Texture.Width/2 , GraphicsDevice.Viewport.Height - 120, 100, 20);

            Components.Add(Paddle);

            /******* Erstellen der Ziegelsteinwand *********/

            ZiegelsteinWand = new List<Ziegelstein>();
            int positionY = 100;
            int positionX = 47;
            int counter = 0;

            for (int i =0; i< anzahlZiegelsteineimSpiel; i++)
            {
                ZiegelsteinWand.Add(new Ziegelstein(this, "Ziegel" + i, "Bilder\\Brick"));
                ZiegelsteinWand[i].Destination = new Rectangle(positionX,positionY,66,30);
                Components.Add(ZiegelsteinWand[i]);

                if(counter >= 13)
                {
                    positionY += 30;
                    positionX = 47;
                    counter = 0;
                }
                else
                {
                    positionX += 66;
                    counter++;
                }

            }


            /********** erstellen des Balls ********/

            Ball = new Ball(this, "Ball", "Bilder\\Ball");
            Ball.Destination = new Rectangle(0, 0, 10, 10);
            Ball.BallReset();
            Components.Add(Ball);

            /******* Erstellen der Anzeige ****/
            anzeige = Content.Load<SpriteFont>("Bilder\\anzeige");

            /**** Laden der Spielende Bilder****/

            gameOverAnzeige = new BasicSpriteComponent(this, "Game over", "Bilder\\Verloren");
            gewonnenAnzeige = new BasicSpriteComponent(this, "Gewonnen", "Bilder\\Gewonnen");

            gameOverAnzeige.Destination = new Rectangle(GraphicsDevice.Viewport.Width / 2 - gameOverAnzeige.Texture.Width / 2,
                                                          GraphicsDevice.Viewport.Height / 2 - gameOverAnzeige.Texture.Height / 2,
                                                           1500, 1500);

            gewonnenAnzeige.Destination = new Rectangle(GraphicsDevice.Viewport.Width / 2 - gewonnenAnzeige.Texture.Width / 2,
                                                          GraphicsDevice.Viewport.Height / 2 - gewonnenAnzeige.Texture.Height / 2,
                                                           1500, 1500);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

        /******* Trifft der Ball etwas??? *************/
            if (Ball.Bounds.Intersects(Paddle.Bounds))
            {
                Ball.Collision(Paddle);
            }

            for(int i = 0; i <ZiegelsteinWand.Count;i++)
            {
                Ziegelstein z = ZiegelsteinWand[i];

                if (Ball.Bounds.Intersects(z.Bounds))
                {
                    Ball.Collision(z);                    
                    Components.Remove(z);
                    ZiegelsteinWand.Remove(z);
                    punktestand++;
                    anzahlZiegelsteineimSpiel--;
                    i--;

                        if( anzahlZiegelsteineimSpiel <= 0)
                        {
                            if (!Components.Contains(gewonnenAnzeige))
                            {
                                Components.Add(gewonnenAnzeige);
                                Components.Remove(Ball);                                
                            }
                        }
                 }
                
            }

            if (Ball.Bounds.Intersects(WandLinks.Bounds))
            {
                Ball.Collision(WandLinks);
            }

            if (Ball.Bounds.Intersects(WandRechts.Bounds))
            {
                Ball.Collision(WandRechts);
            }



            /***** Wenn der Ball unten runter fällt *************/
            if (Ball.Ballout() && Components.Contains(Ball))
            {
                if (leben > 0)
                {
                    leben--;
                    if (leben > 0)
                        Ball.BallReset();
                }
                else if (!Components.Contains(gameOverAnzeige))
                { Components.Add(gameOverAnzeige); }                
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            /***** Anzeigen erstellen ********/
            spriteBatch.Begin();
            spriteBatch.DrawString(anzeige, "Highscore: "+ punktestand.ToString("000"), new Vector2(60, 40), Color.White);
            spriteBatch.DrawString(anzeige, "Leben: "+leben.ToString("0"), new Vector2(GraphicsDevice.Viewport.Width - 120, 40), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
