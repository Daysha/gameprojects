﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RingCollider : MonoBehaviour

{
    public GameObject Draht;
    public Material Red;
    public Material Green;
    public GameObject Parent,endpanel;
    public Text text,endText;
    public GameObject startPoint, endPoint;
    private Vector3 acutePosition;
    private Quaternion acuteRotation;
    private float Punkte;
    private int MaxTimeinMinutes = 10;
    private float actualTime=0;
    private int countSec = 0;
    private int minutes = 0;
    private bool isGamestarted=false;


    // Start is called before the first frame update
    void Start()
    {
        acutePosition = Parent.transform.position;
        acuteRotation = Parent.transform.rotation;
    }
    // Update is called once per frame
    void Update()
    {

        if (isGamestarted)
        {
            setTimer();
        }
        else
        {
            startTimer();
        }

        if (IsInCollider())
        {
            transform.GetComponent<MeshRenderer>().material = Green;
            
        }
        else
        {
            transform.GetComponent<MeshRenderer>().material = Red;
            countSec += 10;
            Parent.GetComponent<Rigidbody>().isKinematic = false;
            Parent.transform.position = acutePosition;
            Parent.transform.rotation = acuteRotation;
        }

        Endgame();
    }

    public bool IsInCollider()
    {
        return GetComponent<Collider>().bounds.Intersects(Draht.GetComponents<Collider>()[0].bounds);
    }

    private void Endgame()
    {
        if (GetComponent<Collider>().bounds.Intersects(endPoint.GetComponents<Collider>()[0].bounds))
        {
            isGamestarted = false;
            Punkte = MaxTimeinMinutes - minutes - (countSec / 100);
            endText.text = "Du hast" + Punkte + "Punkte erhalten";
            endpanel.SetActive(true);
        }    
    }

    public void startTimer()
    {
        if (GetComponent<Collider>().bounds.Intersects(startPoint.GetComponents<Collider>()[0].bounds))
        {
            isGamestarted = true;
        }
    }


    private void setTimer()
    {
        actualTime += Time.deltaTime;
        if (actualTime > 1)
        {
            countSec++;
            actualTime = 0;
        }
        setTimertoText();
    }

    private void setTimertoText()
    {
        string timertext = "";
            
            if (minutes < MaxTimeinMinutes)
            {
                if(countSec > 59)
                {
                    countSec = 0;
                    minutes++;
                }

                if (countSec < 10)
                {
                    timertext = "0" + minutes + ":0" + countSec;
                }
                else
                {
                    timertext = "0" + minutes + ":" + countSec;
                }
            }
            else {
                timertext = "Game Over";
            }

        text.text = timertext;
    }


}
