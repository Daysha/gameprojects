import java.awt.Color;

import javax.swing.JButton;
import javax.swing.border.LineBorder;

public class Button {
	
	JButton btn;
	private int nummer, x, y, fNummer;
// nummer = Nummer des Button
// x = XPosition des Buttons
//Y = Y Positison des Buttons	
	private String name, text;
	
	
	public Button() {
		btn = new JButton();
		btn.setVisible(true);
		//btn.addActionListener(new ActionHandler()); // Vielleicht nicht gebraucht. Action handler = Neue Klasse. 
		btn.setFocusPainted(false);
		btn.setContentAreaFilled(false);
		btn.setBorder(new LineBorder(Color.BLACK));
		btn.setName("free");
		name = btn.getName();
		btn.setText("");
		text = btn.getText(); 
		setColorbyName();
		setFNummer(0);
		
	}
	
	// Getter und Setter
	
	public void setNummer(int n) {
		nummer=n;
	}
	
	public int getNummer() {
		return nummer;
	}
	public void setFNummer(int n) {
		fNummer=n;
	}
	
	public int getFNummer() {
		return fNummer;
	}
	public void setText(String pText) {
		btn.setText(pText);
		text = btn.getText();
	}
	
	public void setPosition(int px,int py,int w,int h) {
		btn.setBounds(px,py,w,h);
	}
	public void setX(int px) {
		x=px;
		
	}
	public void setY(int py) {	
		y=py;
	}
	
	public int getX() {
		return x;
	}
	public int getY() {	
		return y;
	}
		 
		
	public void setColor(Color color) {
		btn.setBackground(color);
	}
	
	public void setName(String pName) {
		btn.setName(pName);
		name = btn.getName();
	}
	
	public String getName() {	
		return name;
	}
	
	//***************** Methoden**********************************************************
	
	// Farbe des Button je nach Namen
	
	public void setColorbyName() {
		
		switch (name) {
		case "start" : setColor(Color.RED);btn.setOpaque(true);break;
		case "ziel" : setColor(Color.GREEN);btn.setOpaque(true);break;
		case "hindernis" : setColor(Color.BLACK);btn.setOpaque(true);break;
		case "weg" : setColor(Color.BLUE); btn.setOpaque(true);break;
		default: setColor(null);
		}
		
	}
	
	
}
