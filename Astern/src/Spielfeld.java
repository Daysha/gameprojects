import javax.swing.JFrame;
import java.util.Random;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.LinkedList;



// vorbild TicTacToe https://www.youtube.com/watch?v=yOJNyrUSe8I
public class Spielfeld {
	
	private int zeilen;
	private int spalten;
	private int groese;
	private int anzHindernisse;
	
	private int punkte;
	private int f, gW, gD, h, startpunkt, zielpunkt, pPunkt, rPunkt;
	
//pPunkt aktueller Positionspunkt
//rPunkt = der zu berechnete Punkt	
//f = Kosten; g = gegangener Weg, h = herablub	

	
	private JFrame jf;
	private LinkedList openFlist = new LinkedList<Integer>();	 
	private LinkedList openPlist = new LinkedList<Integer>();

//	static int[][] felder = new int[zeilen][spalten];
	static Button punkt[];
	private Random random = new Random();
	
	
	// erstellen der Graphischen Oberfl�che und des Spielfeldes
	public Spielfeld(int pZeilen, int pSpalten){
		
		gW=10;
		gD=14;
		h=0;
		f=0;
		
		zeilen=pZeilen;
		spalten=pSpalten;
		groese = zeilen*3;
		punkte = zeilen*spalten;
		punkt = new Button[punkte];
		anzHindernisse=punkte/10;
		
		jf = new JFrame();
		jf.setSize(punkte*3+40,punkte*3+60);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setLocationRelativeTo(null);
		jf.setLayout(null);
		jf.setResizable(true);
		jf.setTitle("A* Sternchen Algorithmus");
		
		erstellePunkte();
		setPunkte();
		setStartpunkt();
		setEndpunkt();
		setHindernisse();
		
		pPunkt=startpunkt;
		System.out.println("Startpunkt ist bei: "+ startpunkt);
		System.out.println("Zielpunkt ist bei: "+zielpunkt);
		
		findeWeg();

		
		jf.setVisible(true);
		
		
	
	}
	
	
	//Erstellen der Punkte
	
	public void erstellePunkte() {
		
		int counter=0;
		
				for(int z=0; z<zeilen; z++) {
					for(int s=0; s<spalten; s++) {
						punkt[counter] = new Button();
						punkt[counter].setText(""+counter);
						punkt[counter].setNummer(counter);
						punkt[counter].setX(s);
						punkt[counter].setY(z);
						jf.add(punkt[counter].btn);	
						counter++;
					}
				}
	}
	
	
// setzen der Punkte.	
	public void setPunkte() {
		int counter=0;
		int x=10;
		int y=10;
		
		
		for(int z = 0; z<zeilen; z++) {

			for(int s = 0; s<spalten; s++) {
				
			Spielfeld.punkt[counter].setPosition(x,y,groese,groese);			
			x=x+groese;	
			counter++;
			}
			x=10;
			y=y+groese;
		}
		
	}
//************************************Setzen der Sonder Punkte *************************************
	
	// setzen des Startpunktes
	
	public void setStartpunkt() {
				
		int feld;
		
		feld = random.nextInt(punkt.length-1);		
		punkt[feld].setName("start");
		punkt[feld].setColorbyName();
		punkt[feld].setText("S");
		startpunkt = feld;
	}
	
	// setzen des Endpunktes
	
	public void setEndpunkt() {
		
		int feld;
		boolean zielpunktOK = false;
		
		while (!zielpunktOK) {
			feld = random.nextInt(punkte-1);
			if (!punkt[feld].getName().equalsIgnoreCase("start")) {
				punkt[feld].setName("ziel");
				punkt[feld].setColorbyName();
				punkt[feld].setText("Z");
				zielpunktOK = true;
				zielpunkt = feld;
			}
		}	
	}

	
//setze Hindernisse
	public void setHindernis() {
		int feld;
		String feldname;
		
		do {feld = random.nextInt(punkte-1);
			feldname = punkt[feld].getName();
			}
		while(feldname.equals("start") || feldname.equals("ziel"));
		
	
		punkt[feld].setName("hindernis");
		punkt[feld].setColorbyName();
	
		}
	
	public void setHindernisse() {
		for(int i =0; i<anzHindernisse; i++) { 
			setHindernis();
		}
	}
	
//************************************ Suche Nachbarpunkt ***********************************
	
// Rechter Nachbar
	public int rechts() {
		
		int x = (punkt[pPunkt].getX())+1;
		int y = punkt[pPunkt].getY();			
	
		for(int i = 0; i<punkte; i++) {	
				if(punkt[i].getX()==x && punkt[i].getY()==y) {
					return i;				
				}
		}				
		return -1;
	}

// Nachbar Oben
	public int oben() {

			int x = (punkt[pPunkt].getX());
			int y = punkt[pPunkt].getY()-1;			
		
			for(int i = 0; i<punkte; i++) {	
					if(punkt[i].getX()==x && punkt[i].getY()==y ) {
						return i;				
					}
			}				
			return -1;
		}
	
// nachbar Unten
	public int unten() {

		int x = (punkt[pPunkt].getX());
		int y = punkt[pPunkt].getY()+1;			
	
		for(int i = 0; i<punkte; i++) {	
				if(punkt[i].getX()==x && punkt[i].getY()==y ) {
					return i;				
				}
		}				
		return -1;
	}
	
//Linker Nachbar
		public int links() {
			
			int x = (punkt[pPunkt].getX())-1;
			int y = punkt[pPunkt].getY();			
		
			for(int i = 0; i<punkte; i++) {	
					if(punkt[i].getX()==x && punkt[i].getY()==y ) {
						return i;				
					}
			}				
			return -1;
		}

//LinkerOberer Nachbar
		public int linksOben() {
					
			int x = (punkt[pPunkt].getX())-1;
			int y = punkt[pPunkt].getY()-1;			
				
				for(int i = 0; i<punkte; i++) {	
						if(punkt[i].getX()==x && punkt[i].getY()==y ) {
							return i;				
						}
				}				
				return -1;
			}
//LinkerUnterer Nachbar
		public int linksUnten() {
					
				int x = (punkt[pPunkt].getX())-1;
				int y = punkt[pPunkt].getY()+1;			
				
				for(int i = 0; i<punkte; i++) {	
					if(punkt[i].getX()==x && punkt[i].getY()==y ) {
						return i;				
						}
				}				
				return -1;
			}

//RechterUnterer Nachbar
		public int rechtsUnten() {
							
			int x = (punkt[pPunkt].getX())+1;
			int y = punkt[pPunkt].getY()+1;			
						
				for(int i = 0; i<punkte; i++) {	
					if(punkt[i].getX()==x && punkt[i].getY()==y ) {
						return i;				
						}
					}				
			return -1;
				}

		
//RechterOberer Nachbar
		public int rechtsOben() {
									
			int x = (punkt[pPunkt].getX())+1;
			int y = punkt[pPunkt].getY()-1;			
								
			for(int i = 0; i<punkte; i++) {	
				if(punkt[i].getX()==x && punkt[i].getY()==y ) {
					return i;				
					}
					}				
			return -1;
					}
//*********************************** Check Ende, wege und Hinderniss********************************
		
//** check ob Waagerecht Ende ist		
		public boolean checkEndeWaage() {
			int r = rechts();
			int l = links();
			int o = oben();
			int u = unten();
			
			if(r == zielpunkt||l == zielpunkt || o == zielpunkt || u == zielpunkt) {
				return true;
			}
			return false;
			
		}
//*** check ob diagonal Ende ist

//** check RechtsOben
		public boolean checkEndeRO() {
			int rO = rechtsOben();
			
			if(rO == zielpunkt) {
				return true;
			}
			return false;
			
		}

//** check RechtsUnten
				public boolean checkEndeRU() {
					
					int rU = rechtsUnten();			
					if(rU == zielpunkt) {
						return true;
					}
					return false;
					
				}
//** check LinksOben
				public boolean checkEndeLO() {
					
					int rU = linksOben();			
					if(rU == zielpunkt) {
						return true;
					}
					return false;
					
				}
				

//** check LinksUnten
	public boolean checkEndeLU() {
					
					int rU = linksUnten();			
					if(rU == zielpunkt) {
						return true;
					}
					return false;
					
				}
//*** check ob rechts ein Hinderniss ist
		public boolean checkRHinderniss() {
			
			String r = "";

			
			if(rechts()!=-1) {r = punkt[rechts()].getName();}

			
			String h = "hindernis";
			String s = "start";
			if(r.equals(h)||r.equals(s)) {
				return true;
			}
			return false;
		}

//*** check ob oben ein Hinderniss ist
		public boolean checkOHinderniss() {									
			String o = "";
			if(oben()!=-1) {o = punkt[oben()].getName();}
					
			String h = "hindernis";
			String s = "start";
			
			if(o.equals(h)|| o.equals(s)) {
				return true;
					}
			return false;
				}

//*** check ob Unten ein Hinderniss ist
		public boolean checkUHinderniss() {
					
			String u = "";					
			if(unten()!=-1) {u = punkt[unten()].getName();}
					
			String h = "hindernis";
			String s = "start";
			
				if(u.equals(h)|| u.equals(s) ) {
					return true;
					}
					return false;
				}
				
//*** check ob Links ein Hinderniss ist
		public boolean checkLHinderniss() {
					
			String l = "";
			if(links()!=-1) {l = punkt[links()].getName();}
					
			String h = "hindernis";
			String s = "start";
					
				if(l.equals(h)|| l.equals(s)) {
					return true;
					}
					return false;
				}
		
//*** check ob rechts ein weg ist
				public boolean checkRWeg() {
					
					String r = "";

					
					if(rechts()!=-1) {r = punkt[rechts()].getName();}

					
					String h = "weg";
					
					if(r.equals(h)) {
						return true;
					}
					return false;
				}

//*** check ob oben ein weg ist
				public boolean checkOWeg() {									
					String o = "";
					if(oben()!=-1) {o = punkt[oben()].getName();}
							
					String h = "weg";					
					if(o.equals(h)) {
						return true;
							}
					return false;
						}

//*** check ob Unten ein Weg ist
				public boolean checkUWeg() {
							
					String u = "";					
					if(unten()!=-1) {u = punkt[unten()].getName();}
							
					String h = "weg";
							
						if(u.equals(h)) {
							return true;
							}
							return false;
						}
						
//*** check ob Links ein Weg ist
			public boolean checkLWeg() {
				String l = "";
				if(links()!=-1) {l = punkt[links()].getName();}
						
				String h = "weg";
						
					if(l.equals(h)) {
						return true;
						}
						return false;
					}
			

			
//*** check ob rechtsOben ein Hinderniss ist
			public boolean checkROHinderniss() {
				
				String r = "";				
				if(rechtsOben()!=-1) {r = punkt[rechtsOben()].getName();}				
				String h = "hindernis";
				String s = "start";
				
				if(r.equals(h)||r.equals(s)) {
					return true;
				}
				return false;
			}

	//*** check ob Linksoben ein Hinderniss ist
			public boolean checkLOHinderniss() {									
				String o = "";
				if(linksOben()!=-1) {o = punkt[linksOben()].getName();}
						
				String h = "hindernis";
				String s = "start";
				if(o.equals(h)||o.equals(s)) {
					return true;
						}
				return false;
					}

	//*** check ob RechtsUnten ein Hinderniss ist
			public boolean checkRUHinderniss() {
						
				String u = "";					
				if(rechtsUnten()!=-1) {u = punkt[rechtsUnten()].getName();}
						
				String h = "hindernis";
				String s = "start";
						
					if(u.equals(h) || u.equals(s)) {
						return true;
						}
						return false;
					}
					
	//*** check ob LinksUnten ein Hinderniss ist
			public boolean checkLUHinderniss() {
						
				String l = "";
				if(linksUnten()!=-1) {l = punkt[linksUnten()].getName();}
						
				String h = "hindernis";
				String s = "start";
				
					if(l.equals(h)||l.equals(s)) {
						return true;
						}
						return false;
					}
			
	//*** check ob rechtsOben ein weg ist
					public boolean checkROWeg() {
						
						String r = "";

						
						if(rechtsOben()!=-1) {r = punkt[rechtsOben()].getName();}

						
						String h = "weg";
						
						if(r.equals(h)) {
							return true;
						}
						return false;
					}

	//*** check ob Linksoben ein weg ist
					public boolean checkLOWeg() {									
						String o = "";
						if(linksOben()!=-1) {o = punkt[linksOben()].getName();}
								
						String h = "weg";					
						if(o.equals(h)) {
							return true;
								}
						return false;
							}

//*** check ob RechtsUnten ein Weg ist
					public boolean checkRUWeg() {
								
						String u = "";					
						if(rechtsUnten()!=-1) {u = punkt[rechtsUnten()].getName();}
								
						String h = "weg";
								
							if(u.equals(h)) {
								return true;
								}
								return false;
							}
							
//*** check ob LinksUnten ein Weg ist
				public boolean checkLUWeg() {
					String l = "";
					if(linksUnten()!=-1) {l = punkt[linksUnten()].getName();}
							
					String h = "weg";
							
						if(l.equals(h)) {
							return true;
							}
							return false;
						}
			
			
// ************************ A Stern Berechnungen ***********************************
	
	//Berechnung wie weit ist Ziel von Start entfernt mit der Mannhatten Methode
	
	public int mannhattenW() {
		int xZ = punkt[zielpunkt].getX();
		int yZ = punkt[zielpunkt].getY();
		int xP = punkt[rPunkt].getX();
		int yP = punkt[rPunkt].getY();
		
		return gW*(Math.abs(xP-xZ)+Math.abs(yP-yZ));
		
	}
	public int mannhattenD() {
		int xZ = punkt[zielpunkt].getX();
		int yZ = punkt[zielpunkt].getY();
		int xP = punkt[rPunkt].getX();
		int yP = punkt[rPunkt].getY();
		
		return gW*(Math.abs(xP-xZ)+Math.abs(yP-yZ));
		
	}
	
	public int aSternW() {
		
		return gW+mannhattenW();
	}

	public int aSternD() {
		
		return gD+mannhattenD();
	}

//*** berechnung  A* der nachbar Punkte
	
	//rechts
	public int rechneRPunkt(){
		rPunkt = rechts();
		
		if(rPunkt!=-1 && !checkEndeWaage() && !checkRHinderniss()) {
			
		int Pnummer = aSternW();
		
		punkt[rechts()].setFNummer(Pnummer);
		punkt[rechts()].setText(""+Pnummer);
		return Pnummer;
		}
		return -1;
	}
	// oben
	
	public int rechneOPunkt(){
		rPunkt = oben();
		
		if(rPunkt!=-1  && !checkEndeWaage() && !checkOHinderniss() ) {
			
		int Pnummer = aSternW();;
		
		punkt[oben()].setFNummer(Pnummer);
		punkt[oben()].setText(""+Pnummer);
		
		return Pnummer;
		}
		return -1;
	}
	
	// unten
	
	public int rechneUPunkt(){
		rPunkt = unten();
		
		if(rPunkt!=-1  && !checkEndeWaage() && !checkUHinderniss()) {
			
		int Pnummer = aSternW();
		
		punkt[unten()].setFNummer(Pnummer);
		punkt[unten()].setText(""+Pnummer);
		
		return Pnummer;
		}
		return -1;
	}
	
	//links
	
	public int rechneLPunkt(){
		rPunkt = links();
		
		if(rPunkt!=-1 && !checkEndeWaage() && !checkLHinderniss()) {
	
		int Pnummer = aSternW();
		
		punkt[links()].setFNummer(Pnummer);
		punkt[links()].setText(""+Pnummer);
		
		return Pnummer;
		}
		return -1;
	}
	
//rechtsOben
		public int rechneROPunkt(){
			rPunkt = rechtsOben();
			
			if(rPunkt!=-1 && !checkEndeRO() && !checkROHinderniss()) {
				
			int Pnummer = aSternD();
			
			punkt[rechtsOben()].setFNummer(Pnummer);
			punkt[rechtsOben()].setText(""+Pnummer);
			return Pnummer;
			}
			return -1;
		}
// Linksoben
		
		public int rechneLOPunkt(){
			rPunkt = linksOben();
			
			if(rPunkt!=-1  && !checkEndeLO() && !checkLOHinderniss() ) {
				
			int Pnummer = aSternD();
			
			punkt[linksOben()].setFNummer(Pnummer);
			punkt[linksOben()].setText(""+Pnummer);
			
			return Pnummer;
			}
			return -1;
		}
		
// Rechtsunten
		
		public int rechneRUPunkt(){
			rPunkt = rechtsUnten();
			
			if(rPunkt!=-1  && !checkEndeRU() && !checkRUHinderniss()) {
				
			int Pnummer = aSternD();
			
			punkt[rechtsUnten()].setFNummer(Pnummer);
			punkt[rechtsUnten()].setText(""+Pnummer);
			
			return Pnummer;
			}
			return -1;
		}
		
//linksUnten
		
		public int rechneLUPunkt(){
			rPunkt = linksUnten();
			
			if(rPunkt!=-1 && !checkEndeLU() && !checkLUHinderniss()) {
		
			int Pnummer = aSternD();
			
			punkt[linksUnten()].setFNummer(Pnummer);
			punkt[linksUnten()].setText(""+Pnummer);
			
			return Pnummer;
			}
			return -1;
		}


//**** Hinzuf�gen der Nachbarn zur Open List

	public void setOpenlist() {
		 openFlist.clear();
		 openPlist.clear();
		int r = rechneRPunkt();
		int o = rechneOPunkt();
		int u = rechneUPunkt();
		int l = rechneLPunkt();
		
		int rO = rechneROPunkt();
		int lO = rechneLOPunkt();
		int rU = rechneRUPunkt();
		int lU = rechneLUPunkt();
					
			
		
		if(!checkEndeWaage()) {
		if(r!=-1&&!checkRHinderniss()&&!checkRWeg()) {
			openFlist.add(r);openPlist.add(rechts());
			}
		
		if(o!=-1&&!checkOHinderniss()&&!checkOWeg()) {
			openFlist.add(o);openPlist.add(oben());
			}
		
		if(u!=-1&&!checkUHinderniss()&&!checkUWeg()) {
			openFlist.add(u);openPlist.add(unten());
			}
		
		if(l!=-1&&!checkLHinderniss() && !checkLWeg()) {
			openFlist.add(l);openPlist.add(links());
			}
		}
		
			if(rO!=-1&&!checkROHinderniss()&&!checkROWeg()&&!checkEndeRO()) {
				openFlist.add(rO);openPlist.add(rechtsOben());
				}
			
			if(lO!=-1&&!checkLOHinderniss()&&!checkLOWeg()&&!checkEndeLO()) {
				openFlist.add(lO);openPlist.add(linksOben());
				}
			
			if(rU!=-1&&!checkRUHinderniss()&&!checkRUWeg()&&!checkEndeRU()) {
				openFlist.add(rU);openPlist.add(rechtsUnten());
				}
			
			if(lU!=-1&&!checkLUHinderniss() && !checkLUWeg() && !checkEndeLU()) {
				openFlist.add(lU);openPlist.add(linksUnten());
				}
			
		}
	
	

//*** Checken welcher Punkt am n�chsten zum Ziel ist
	// check Kleinsten F wert
	
	public int setKleinsterF() {
		setOpenlist();
		int loop = openFlist.size();
		int check = (int)openFlist.get(0);
		

			for(int i =0; i<loop; i++) {
			if(check>(int)openFlist.get(i)) {
				check=(int)openFlist.get(i);				
				}
			}

		return check;
	}
	
	// finde Punkt mit kleinster Nummer und setze den als pPunkt
	
	public void nextPunkt() {
		int check = setKleinsterF();
		int loop = openPlist.size();
		
			for(int i = 0; i<loop; i++) {

				if(punkt[(int)openPlist.get(i)].getFNummer()==check) {				
				pPunkt = punkt[(int)openPlist.get(i)].getNummer();
				
				punkt[pPunkt].setName("weg");
				punkt[pPunkt].setColorbyName();
								
				break;
				}				
			}
						
	}
	
	public void findeWeg(){
		int counter=0;
		
		do {

			nextPunkt();			
			System.out.println("nehme den weg durch:"+pPunkt);
			counter++;
			
		}
		while(!checkEndeWaage()&& counter<punkt.length);
		}
	}
	
	




